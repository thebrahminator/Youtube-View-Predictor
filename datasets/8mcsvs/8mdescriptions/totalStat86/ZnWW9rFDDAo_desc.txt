Codex Las Huelgas, siglo XIV
Ave maris stella (Conduit)
Performers: Discantus


Ave maris stella,
Virgo decus virginum,
Celi regis cella,
Mediatrix hominum.
Ne nos pereamus,
Ad te suspiramus,
Et a nobis criminum
Procul sit procella.


Ave maris stella (conduit)
Hail, Mary, star of the sea,
O Virgin, jewel of virgins,
Enclosed garden of the king of heaven,
Mediator, for men,
Lead us not astray,
Towards you we sigh,
May the storms of our misdeeds
Be removed far from us.