Stop, Drop and POP!  This is the first Music Video released by artist Dree Paterson for single
Waiting (When I See You).

Download the song on Itunes: 
https://itunes.com/dreepaterson


Booking: dreepaterson@gmail.com 

For All things Dree: 

Facebook:
www.facebook.com/dreepatersonsounds

Twitter:
www.twitter.com/dreepaterson 

Instagram @dreepaterson 

Vine @dreepaterson

Soundcloud:
www.soundcloud.com/dreepaterson

Directed by Donte Murry - https://www.facebook.com/kiteflightfilms
Song Produced and Engineered at - The Cave Studios, Los Angeles CA
Waiting (When I See You) written by Dree Paterson and Victor Garcia
Adriani Paterson Publishing - Sesac 

Thank you all for watching my sweethearts! 

DON'T FORGET TO SUBSCRIBE :) 

Music video by Dree Paterson performing Waiting (When I See You). (P) (C) 2012  All rights reserved. Unauthorized reproduction is a violation of applicable laws.