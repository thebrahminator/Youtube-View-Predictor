BIS presented an alpha build of Arma 3 at this year's E3, demonstrating a big variety of both technical and gameplay improvements, across a number of unique showcases!

This Night Ops Showcase takes a closer look at some of the great enhancements to lighting, and demonstrates the way that lots of little features work together to create even better gameplay opportunities in a short fire fight.