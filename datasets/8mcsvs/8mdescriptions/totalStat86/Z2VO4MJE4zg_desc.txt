lyrics of Vietnamese Girl with yellow skin song (Trinh Cong Son)

Girl, so young, with skin like gold,
Home you love like fields of grain,
Girl, so young, with skin like gold,
On your face fall tears like rain. 

Girl, so young, with skin like gold,
Home you love, so do love the weak.
Seated there in dreams of peace,
Proud of home as of your womanhood. 

You've never known our land in peace.
You've never known Olden Viet Nam.
You've never sung our village songs.
All you have is an angry heart. 

Passing by the village gate,
In the night with guns booming low,
Girl so young, you clutch your heart.
On soft skin a bleeding wound grows. 

Girl, so young, with skin like gold,
Home you love like fields of grain,
Girl, so young, with skin like gold,
You love home which is no more. 

O! Unfeeling and heartless death.
Dark our land a thousand years.
Home, my sister, you've come alone.
And I, alone, still search for you. 

Girl, so young, with skin like gold
(3 times, fading)
************************************
Ngi con gi Vit Nam da vng (Trinh Cong Son)

Ngi con gi Vit Nam da vng
Yu qu hng nh yu ng la chn,
Ngi con gi Vit Nam da vng,
Yu qu hng nc mt lng trng.

Ngi con gi Vit Nam da vng
Yu qu hng nn yu ngi yu km.
Ngi con gi ngi m thanh bnh.
Yu qu hng nh  yu mnh. 

Em cha bit qu hng thanh bnh.
Em cha thy xa kia Vit Nam.
Em cha ht ca dao mt ln.
Em ch c con tim cm hn. 

Ngi con gi mt hm qua lng.
i trong m, m vang m ting sng.
Ngi con gi cht m tim mnh.
Trn da thm vt mau loang dn. 

Ngi con gi Vit Nam da vng,
Yu qu hng nh yu ng la chn.
Ngi con gi Vit Nam da vng
Yu qu hng nay  khng cn. 

i ci cht au thng v tnh!
i t nc u m ngn nm!
Em  n qu hng mt mnh,
Ring ti vn u lo i tm. 

Ngi con gi Vit Nam da vng
(3 times fading)