Holiman y Ube y su interpretacin y arreglo de la cancin popular de los aos 50 escrita por Buck Ram e interpretada por The Platters. 
Una adaptacin a la bachata con letra en ingls y espaol. 

Holiman y Ube and their interpretation and arrangement of the popular 50's song Only You; composed by Buck Ram and performed by The Platters. This version includes English as well as Spanish lyrics, some translated some original and it's in the popular latin genre Bachata.

Watch The Official Video Here / El Video Oficial Aqu 
https://youtu.be/kNBNXKMG9hk

Watch The Latest Music Video/ Haz CLick Para Ver El Video Musical Ms Reciente
https://youtu.be/3UrV2cWyXWc

www.holimanube.com
www.facebook.com/holimanube
www.reverbnation.com/holimanube
www.soundcloud.com/elcaminoprod
www.twitter.com/holiman_ube
www.youtube.com/holimanube

contact: holimanube@gmail.com

Letra / Lyrics
Only You, can make this world seem right 
Only You, can make the darkness bright

Only You, can make this world seem right 
Only You, can make the darkness bright 
Solo tu y tu amor 
Me hacen sentir vivo 
Y llenas tu, mi corazn 
Solo tu, haces un cambio en mi 
For it's true, you are my destiny 
When you hold my hand 
I understand 
La magia que hay en ti 
Eres un sueo amor 
Y solo tu

When you hold my hand 
I understand 
La magia que hay en ti 
Eres un sueo amor 
Y solo tu

Solo tu mi amor mi sueo 
Eres luz a mi camino 
Solo tu mi amor mi sueo 
Eres paz y mi destino 
Tu y Solo Tu 
Eres la duea 
De todo mi cario

Solo tu, haces un cambio en mi 
For it's true, you are my destiny 
When you hold my hand 
I understand 
La magia que hay en ti 
Eres un sueo amor 
Y solo tu 
Only You

One and Only You