This is a collaborative effort between Fishfood2021 (DJ Parkour) and UncleSqueezins (DJ Deimos)

-=Lyrics=-

There's a wild wind blowing
Down the corner of my street
Every night there the headlights are glowing

There's a cold war coming
On the radio I heard
Baby it's a violent world

Oh love don't let me go
Won't you take me where the streetlights glow
I could hear it coming
I could hear the sirens sound
Now my feet won't touch the ground

Time came a-creepin'
Oh and time's a loaded gun
Every road is a ray of light
It goes o-o-on
Time only can lead you on
Still it's such a beautiful night

Oh love don't let me go
Won't you take me where the streetlights glow
I could hear it coming
Like a serenade of sound
Now my feet won't touch the ground

Gravity release me
And dont ever hold me down
Now my feet won't touch the ground.

www.myspace.com/realdjparkour - MUSIC MYSPACE
ww.myspace.com/fishfood2021 - PERSONAL MYSPACE
http://unclesqueezins.newgrounds.com/ - UncleSqueezins/DJ Deimos