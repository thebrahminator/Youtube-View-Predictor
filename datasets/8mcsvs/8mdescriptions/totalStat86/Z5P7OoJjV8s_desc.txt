This is a video from me, DarkspinesSonic A.K.A Da1AndOnlySonic, showing a speed run of the challenge "Beat the Clock" for Knight's Passage on Sonic and the Black Knight. The longest speed run in the game (as far as missions are concerned, not characters) that's tracked on TSC, and I'm happy with this! Enjoy! ^_^

Join "The Sonic Center" for some real competition in a variety of Sonic games. http://www.soniccenter.org/

Twitch: https://www.twitch.tv/DarkspinesSonic
Twitter: https://www.twitter.com/DarkspinesSonic
Donate: https://youtube.streamlabs.com/UCYbJSdiX5WEmY2l1RpRzU0Q/