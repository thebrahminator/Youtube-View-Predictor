Check out http://bikeaccessoriesreviews.com/reviews/video-shimano-spd-sl-road-cycling-cleats-sm-sh11/ for further information on how to use cycling road shoes.

This review demonstrates the use of road cycling shoes with the Shimano SPD-SL cleat system (SM-SH11) and the Shimano PD-5700 pedals (the 105 series). It is demonstrated on a pair of Nike carbon fiber road cycling shoes.

ALWAYS read the manual before you try to use your bike with a cleat system. Using it not in a proper way can cause injuries!