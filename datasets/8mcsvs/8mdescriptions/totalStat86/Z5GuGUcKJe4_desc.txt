Hello everyone and welcome to QuicksaveTV!

This tutorial is inspired by one of the suggestions made by subscribers on the channel page. In here I will try to tell you as much information as possible about Housing in Skyrim, without spoiling the main story progression.

The example of Skyrim's Real Estate will be Whiterun house this time and I hope it'll be reasonably helpful!

Hope you enjoy the video and have a great day!

=-=-=

As always, if you like the video and enjoy my commentary go ahead like, subscribe and share with your friends! 

Leave a comment and propose your ideas for future content (and get an answer for your questions)!

To contact me, send a message to: 
quicksavetv@gmail.com