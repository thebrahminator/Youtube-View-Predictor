New Sense is the first track to be taken from From Safer Place; the debut long-player from York-based punk trio Fawn Spots. The LP is set for release in March 2015 on Critical Heights.

Buy 'New Sense' here : http://badpaintings.bigcartel.com/product/fawn-spots-7-new-sense-twenty-four-hours-joy-division-cover