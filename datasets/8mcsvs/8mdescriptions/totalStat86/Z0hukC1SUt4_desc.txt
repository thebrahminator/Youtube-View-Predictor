See the video response too, shown below.  This is what was happening at Trevor's house.
http://www.youtube.com/watch?v=lFnQUluP-d8

This started out as wondering if such a thing could work.

Playing Trevor's Wurlitzer Style EX theatre organ over 120 miles away in Michigan.  All local stop tab changes, expression shoe positions and even 2nd touch were mirrored and playing live on his Wurlitzer.  

We were on Skype audio and video during the test. I could see the shades and tabs move at Trevor's house. Audio of the Wurlitzer could be heard but there was about a 0.5 - 1 second delay over Skype. Made it a little hard to play so I had organ sounds on Hauptwerk (Neil Jensen 3/11 Theatre Organ) playing locally to hear something. HW is heard in this video.

Hauptwerk was my relay and Trevor used jOrgan and MidiBox connected to his Wurlitzer.