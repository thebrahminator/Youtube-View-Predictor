Follow & request covers on Twitter @OttoMadCraft - http://twitter.com/ottomadcraft

Otto from MadCraft owning... LeBron James with a drum cover of the song Rise Against - Injection.

We do not own any rights to the background audio. Rise Against "Injection" from the album "Sufferer & the Witness" is the copyrighted property of its owner(s).

External Drums by: Otto Uotila of MadCraft.

Watch Otto play Millencolin - No Cigar http://youtu.be/wXUJWBoW3lc
Watch Otto play All Time Low - The Reckless And The Brave: http://youtu.be/TFxcv7FtX34
Watch Otto play Blink-182 - Dumpweed: http://youtu.be/ISUltS3nI_g
Watch Otto play Blink-182 - The Rock Show: http://youtu.be/OSVjytdhwuk
Watch Otto play All Time Low - Break Your Little Heart: http://youtu.be/PTNsI1jijKA
Watch Otto play MadCraft - All I Want (Is You) http://youtu.be/pX0S52H75Jc

Like MadCraft on Facebook: http://www.facebook.com/MadCraftOfficial
Follow on Twitter: http://twitter.com/WorldOfMadCraft
Circle on Google+: https://plus.google.com/101411377894207658251

For moar check out http://madcraft.net/