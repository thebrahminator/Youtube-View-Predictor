Potato Curry for Masala Dosa, also called as dry Potato Curry/ Aloo Subzi.
https://www.youtube.com/watch?v=ZnBcrPOf3Nk
Ingredients Required
Boiled and Smashed Potato 2-3 Nos
Chopped Onion 1-2 Nos
Chopped Green Chillies 2-3 Nos
Curry leaves 8-10 Nos
Mustard seeds 1 Tea Spoon
Cumin seeds 1 Tea Spoon
Turmeric Powder 1 Tea Spoon
Lemon  No
Urad dal 1 Tea Spoon
Salt 1 Tea Spoon
Oil 1 Table Spoon


Method to prepare Potato Curry/Aloo Subzi
1. Keep the pan for heating and add 1 table spoon oil, once oil is heated add 1 tea spoon Urad dal and fry it till it becomes golden brown.
2. Now add Mustard seeds, cumin seeds, green chillies, curry leaves and fry it for 1 minutes.
3. Now add chopped onion and fry it for 2-3 minutes, add Turmeric powder and salt to taste and fry it again.
4. Add Boiled potato and mix it well, close the container and cook for 2 minutes.
5. Now add coriander leaves, squeeze  lemon and mix it well.


Potato curry is ready now.

It can be used for preparing Masala dosa or can also be had along with Chapati.

Follow us on facebook
https://www.facebook.com/IndianVegetarianDishes