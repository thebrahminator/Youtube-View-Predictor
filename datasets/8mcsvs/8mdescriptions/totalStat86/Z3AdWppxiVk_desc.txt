In part 2 of the Custom Field Knife Series, Adam introduces you to the Heritage Blades Echo 7 (semi-production) from Dan Eastland / Dogwood Custom Knives.  Heritage Blades is the semi-production line from Dan.

Echo 7 Specs:
Steel - O1, but Stainless coming soon
Edge - Scandi
Price - around $200
Length - 8.5 overall
Blade length - almost 4 inch
Blade Thickness - 1/8"
Handle length - just over 4.5
Handle thickness - about 1.5 inches at its widest
Sheath - Reliance Leatherworks


------------------------------------------------------
Equip 2 Endure
http://www.Equip2Endure.com
Join our community today to be part of our contest & giveaways, join discussions on the forums, news, blog, videos, podcasts and so much more!

E2E Store  
http://equip2endure.mybigcommerce.com

Follow Us on Twitter 
http://www.twitter.com/Equip2Endure 

Like Us!  
http://www.facebook.com/Equip2Endure

E2E Newsletter Sign-Up
http://eepurl.com/mZRO9

E2E Newsletter Archives
http://www.equip2endure.com/ViewForumTopic.aspx?id=4536175455924ee3a045b25f5c2370f7