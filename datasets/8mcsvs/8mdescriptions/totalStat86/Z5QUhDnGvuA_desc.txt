Weird Colored Objects Floating above Moscow, Russia.

 ALL-IN-ONE (PLAYLIST): https://goo.gl/cKWSGK
 RECOMMENDED VIDEO: https://youtu.be/XAjFEsmU1nU

 Subscribe To The Backup Channel: https://www.youtube.com/c/findingufo2


 INTERESTING TO WATCH 
----------------------------------------------------------

 UFO Sighting with Giant Spiral over Russia
https://youtu.be/XAjFEsmU1nU

 UFO Sighting with Sparkling Light in Moscow, Russia
https://youtu.be/Dg4L4UywCvM

 Possible UFO Crash in Sverdlovsk Region of Russia in 1969
https://youtu.be/p3IVpOGe2OU


 VIDEO PLAYLISTS 
----------------------------------------------------------

ALL-IN-ONE  https://goo.gl/cKWSGK
UFO & ALIEN NEWS  https://goo.gl/OlJwRa
AREA 51  https://goo.gl/Pt8MiB

GOOGLE EARTH SECRETS  https://goo.gl/fU8zUM
INTERVIEWS & DOCUMENTARIES  https://goo.gl/jLgEjL
UFOs in SPACE  https://goo.gl/aUC83S


 SOCIAL MEDIA 
----------------------------------------------------------

TWITTER  https://twitter.com/FindingUFO
FACEBOOK  https://facebook.com/FindingUFO
GOOGLE+  https://plus.google.com/+FindingUFO

WEBSITE  http://FindingUFO.tv


 CONTACT & COPYRIGHTS 
----------------------------------------------------------

You can contact us for any questions (by example: using one of our uploads for your website or YT-channel) or to submit a sighting on this channel. Contact us thru email at: contact@findingufo.tv

If you noticed a video that violates your COPYRIGHTS, then please inform us and we will DELETE it. Most of the videos we receive thru email from submitters, so it's not always easy to check on COPYRIGHTS or the RIGHT OWNER. Thanks for your understanding and cooperation.