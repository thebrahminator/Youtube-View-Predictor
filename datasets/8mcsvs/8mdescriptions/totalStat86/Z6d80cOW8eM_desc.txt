DOWNLOAD: http://goo.gl/lbFmcS 
VINYL CD: http://goo.gl/QOsoex 

Artist: Blastromen
Song: Sidtroen
Album: Reality Opens
Label: Dominance Electricity

 Buy hi-quality MP3s from:
 Bandcamp: http://dominanceelectricity.bandcamp.com/album/reality-opens
 iTunes: https://itunes.apple.com/album/id818536195
 Beatport: http://www.beatport.com/release/reality-opens/1247389
 Juno: http://www.junodownload.com/products/2409433-02/
 Amazon Germany: http://www.amazon.de/Reality-Opens-Blastromen/dp/B00IEGQ71S
 Amazon France: http://www.amazon.fr/Reality-Opens-Blastromen/dp/B00J0BO3DA
 Amazon UK: http://www.amazon.co.uk/Reality-Opens-Blastromen/dp/B00IEIBVG2
 Amazon Spain: http://www.amazon.es/Reality-Opens-Blastromen/dp/B00IEIE1OG
 Amazon Italy: http://www.amazon.it/Reality-Opens-Blastromen/dp/B00IEGXKFY
 Amazon USA: http://www.amazon.com/Reality-Opens-Blastromen/dp/B00IEG2BLI
 Amazon Japan: http://www.amazon.co.jp/Reality-Opens-Blastromen/dp/B00IEHLJD8

 Buy collectors edition 2x12" VINYL +super-size POSTER:
 Buy hi-quality pressed CD (Digipak):
http://www.saveoursounds.net/en/tag/product/list/tagId/4/



Album release info: 
Blastromen "Reality Opens" (Dominance Electricity, DE-020)

The second coming! Finlands otherworldly Electro lords Blastromen return to the Dominance Electricity mothership to deliver their much anticipated second longplayer.

The eight new compositions featured on "Reality Opens" continue where their debut album "Human Beyond" left off in the year 2010 and lead straight to the next level.

Sophisticated arangements with epic synth melodies and atmospheres, haunting Electro breakbeats and elaborate vocoderized lyrics prove once again that Blastromen are a class of their own.

Packaged in surrealistic artwork by The Zonders, the album is available on all digital platforms as well as Digipak CD, black double 12 inch vinyl and premium edition transparent color double vinyl including a super-size poster.

Album tracklist: 
01. Reality Opens
02. Vision Control
03. Infiltrator Unit
04. Sidtroen
05. Sense Of Ears
06. Glacier Planet
07. Transportation Nonstop
08. Lost Dissents



Dominance Electricity links: 
http://www.dominance-electricity.de
http://www.facebook.com/DominanceElectricity
http://www.twitter.com/Dominance_Rec
http://www.soundcloud.com/Dominance-Electricity

Blastromen links:
http://www.blastromen.com
http://www.facebook.com/Blastromen
http://www.twitter.com/Blastromen
http://www.soundcloud.com/Blastromen