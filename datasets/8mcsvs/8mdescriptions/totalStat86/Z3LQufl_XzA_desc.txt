I've recently been watching some old home movies of me when I was a wee child, including me and my Thomas tapes. I felt mixed emotions, mostly sad and nostalgic. So, not long after that, I started to work on this. I must say, this is by far my favorite video yet. The song is perfect and I hope you think so too.

Please comment, rate, subscribe and Enjoy!
-Matt