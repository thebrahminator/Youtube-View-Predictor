13. november 1996.a.

Andorra La Vella. Estadi Comunal dAixovall.

Spruskohtumine

ANDORRA -- EESTI 1:6

Peakohtunik: Celino Gracia Redondo (Hispaania)
Pealtvaatajaid: 1500

Vravad: 60. Agusti Pol -- 36. Indrek Zelinski, 64., 74., 76., 84. Argo Arbeiter, 87. Marko Kristal

Eesti koondis:
Mart Poom
Urmas Kirs
Marek Lemsalu
Raivo Nmmik
Urmas Rooba (63. Gert Olesk)
Meelis Rooba (76. Viktor Alonen)
Mati Pari (69. Janek Meet)
Marko Kristal
Martin Reim (82. Vahur Vahtrame)
Argo Arbeiter (85. Liivo Leetma)
Indrek Zelinski

Treener: Teitur Thordarson
 
Andorra koondis:
Alfonso Sanchez
Grard Juanez Daz Calvet
Francesc Pedro Lpez
Josep Moraques "Cholo"
Francesc Obiols
Agusti Pol Perez
Johnny Rodriquez (88. Albert Carnice Company)
Josep Felix Alvarez Blasques
Jordi Lamelas Puertas (67. Angel Martin Garcia)
Jesus Julin Lucendo
Juli Sanchez Soto (10. Carlos Medina, 46. Cristbal Aranda, 71. Jorge Bazan)

Treener: Isidro Codina Garcia

Huvitavad faktid:
  Eesti jalgpallikoondise suurim vit taasiseseisvusmisajal. Esimese vabariigi ajal alistas Eesti koondis kahel korral Leedu tulemusega 6:0.
  Argo Arbeiter pstitas korraga mitu rekordit. Esimese Eesti koondise mngijana li ta hes kohtumises 4 vravat, tehes seda 20 minutiga.
  Andorra koondis pidas oma esimese rahvusvahelise maavistluse.