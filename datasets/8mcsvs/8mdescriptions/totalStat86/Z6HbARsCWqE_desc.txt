Music video for my Halloween song "ten thirty one".

This song is my love letter to Halloween and all the nostalgia and emotions the fall season brings. Inspired by my memories of growing up in my old neighborhood, the leaves changing, carving jack o lanterns, my mom dressing up like a witch, the homemade costumes she made for me and my brothers, watching scary movies, and everything else that comes with the Halloween season. There's even a nod to "John Carpenter's Halloween", one of my favorite movies of all time. I tried my best to capture visually what I saw in my mind when I recorded the song.

I hope watching it triggers the same nostalgia and emotions that I felt writing it. 

If you enjoy the song, it will be on my upcoming 3rd album, "Heaven Help Us".  Stream it on bandcamp: https://theghostinyou.bandcamp.com/tr...
or through my website: http://www.theghostinyou.net

Happy Halloween, everybody.

Written, recorded, directed, shot, and edited by Billy Polard in Glenside Pa http://www.theghostinyou.net
Skeleton man played by my oldest friend, Brett Schmoltze
Old family photos by my Aunt Connie
Filmed in October 2014