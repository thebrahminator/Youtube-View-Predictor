Subscribe for more hardware videos! http://www.youtube.com/subscription_center?add_user=pcper

Full story link: http://www.pcper.com/reviews/Displays/Acer-XB280HK-28-4K-G-Sync-Monitor-Review

Buy on Amazon: SOON

Here they come - the G-Sync monitors are finally arriving at our doors! A little over a month ago we got to review the ASUS ROG Swift PG278Q, a 2560x1440 144 Hz monitor that was the first retail-ready display to bring NVIDIA's variable refresh technology to consumers. It was a great first option with a high refresh rate along with support for ULMB (ultra low motion blur) technology, giving users a shot at either option.

Today we are taking a look at our second G-Sync monitor that will hit streets sometime in mid-October with an identical $799 price point. The Acer XB280HK is a 28-in 4K monitor with a maximum refresh rate of 60 Hz and of course, support for NVIDIA G-Sync.

The Acer XB280HK, first announced at Computex in June, is the first 4K monitor on the market to be announced with support for variable refresh. It isn't that far behind the first low-cost 4K monitors to hit the market, period: the ASUS PB287Q and the Samsung U28D590D both shipped in May of 2014 with very similar feature sets, minus G-Sync. I discussed much of the general usability benefits (and issues) that arose when using a consumer 4K panel with Windows 8.1 in those reviews, so you'll want to be sure you read up on that in addition to the discussion of 4K + G-Sync we'll have today.