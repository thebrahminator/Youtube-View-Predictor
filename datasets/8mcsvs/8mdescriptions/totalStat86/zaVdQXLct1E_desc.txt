Not Afraid is one of the tracks in Heart of God Churchs first EP, Yahweh, which was released in December 2014.

This song is available for purchase on the iTunes Store http://itunes.apple.com/album/id962454090 , Google Play Music and Amazon Music. 

For lyrics, chords and lead sheets, visit http://www.heartofgodchurch.org/worship 

Official Music Videos for the Yahweh Album:
Yahweh: https://youtu.be/8vJrmQqTlv0
Not Afraid: https://youtu.be/o2VlHel0yRg

Lyric Videos from the Yahweh Album
Yahweh: https://youtu.be/IKToGnFqhrM
Blessed: https://youtu.be/bqPmNJwNt3I
Blessed (Acoustic): https://youtu.be/86zmTxOsgos
Staying in Your Light: https://youtu.be/6M9QpsBFghg
Not Afraid: https://youtu.be/zaVdQXLct1E

Heart of God Church (HOGC) is an independent youth church in Singapore founded by Pastor Tan Seow How and Pastor Cecilia Chan in 1999. For more information visit http://www.heartofgodchurch.org






Credits:
Some images in this video were taken from http://www.shutterstock.com