**Check out my new My Little Pony review channel on Youtube - just go to http://www.youtube.com/pbeforei and subscribe.  Thanks for your support!!**

A wonderful figure - only one small flaw here to be found... but I can look past it, because it's Vinyl Scratch !!!   A must have for Friendship Is Magic collectors.  

Subscribe to support my channel!  Just click subscribe, give a thumbs up if you like it, and feel free to leave comments.  I thank each of you for watching and supporting my videos!

You can view all of my toy reviews at:
www.youtube.com/ebeforeinet
www.ebeforei.net
www.facebook.com/ebeforei

The sole purpose of this channel and video are for video reviews of toys and electronics, which includes commentary and critique.  It is protected under fair use and does not infringe on copyright in any way.