Stand up comedy written and performed by Danny Mendlow on July 9th, 2008.

Dedicated to the everlasting memory of George Carlin, the man who taught me that vulgarity and substance can co-exist, and that without the latter, the former is meaningless.

May I ever be 1/10000th the human you were, and leave behind 1/1000000000th the impression on this little rock.