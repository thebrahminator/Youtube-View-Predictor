search for free and swap items with ecofreek from craigslist and 45+more classified sites.

ecofreek.com is a search engine that searches the web for free and 'for swap/trade' items people no longer need from over 45+ sources, providing the most diverse and accurate results anywhere in the world. Our mission is to provide a means for people to find items they need while reducing landfill waste.