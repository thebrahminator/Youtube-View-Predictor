PILOT video for a series of film shorts and slideshows demonstrating skateboard skills along with how to improve the Corpus Christi Skatepark. 

Shot and edited by skaters, such videos will break traditional educational barriers in the future by ASSIGNING STUDENTS THE TASK OF TEACHING DESIRED SKILLS AND OBJECTIVES to their peers and community. 

IMMEDIATE OBJECTIVE: To finish the park and up-class it to a popular entertainment venue for 'all-ages' and visitors of the city.

LONG TERM OBJECTIVE: The most exciting aspect of this venture is it's potential to instill rare confidence and vision in young adults by providing them successful experience in the shaping of attitudes and responsible action in their community. 

SEE ALSO:
video modeling
ccskatepark.com