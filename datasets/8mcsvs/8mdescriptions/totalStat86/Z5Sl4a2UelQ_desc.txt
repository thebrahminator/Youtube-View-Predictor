Maruti Suzuki Swift XDi [Diesel]

FEATURES :
1.Double-DIN music system with USB & AUX, 
2.Front power windows, 
3.Art-leather seat covers, 
4.Tilt-steering, 
5.Key-less entry, 
6.Rear parking sensors, 
7.Special door sills
8.Body Side Moldings
9.Side/Wing mirrors with turn indicators (Black Color)
10.Front fog lamps.
11.Front Speakers
12.Wheels Covers

Note - Swift XDi is Based on the base Swift LDi/LXi, with above Features Included, available in Diesel & Petrol.

 ---------------------------------------------------------------------------------------------

MY ADDITIONS :
A. Body color - Door handles & Side/Wing mirrors
B. Rare Power windows
C. Rare Speakers

*Not included in Swift XDi package, Purchased & added separately.

-----------------------------------------------------------------------------------------------

REFERENCES :

http://overdrive.in/news/maruti-suzuki-launches-the-swift-xdi-edition-in-rajasthan-starting-at-rs-5-44-lakh/

http://www.carwale.com/news/11228-maruti-suzuki-swift-xdi-edition-launched-in-rajasthan-for-rs-544-lakh-.html