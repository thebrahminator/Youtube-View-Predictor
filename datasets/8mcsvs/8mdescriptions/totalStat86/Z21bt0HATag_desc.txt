Luton 5-1 Southport
Luton came from behind to thrash 10-man Southport.

Jonathan Brown gave the visitors an early lead but Adam Watkins finished neatly to level for Luton. 

Southport's Steve Akrigg was sent off for a second yellow card, before Will Antwi tapped in Dean Beckwith's header to put the Hatters ahead. 

Amari Morgan-Smith's deflected effort made it 3-1, with Robbie Willmott's spectacular 25-yard strike and Danny Crow's header completing the rout. 

Source:http://news.bbc.co.uk/sport1/hi/football/14590685.stm