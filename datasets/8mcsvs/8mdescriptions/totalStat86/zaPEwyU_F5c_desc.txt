The University of Westminster is pressing ahead with an exciting programme of improvements to its buildings and surroundings. This is part of a ten-year strategy to invest in our estate, which began three years ago.

Find out more: http://www.westminster.ac.uk/about-us/university-developments