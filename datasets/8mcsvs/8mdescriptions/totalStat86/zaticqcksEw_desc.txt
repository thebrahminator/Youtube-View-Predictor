Slovenia could breath a sigh of relief after letting a 15-point lead slide before hanging on for a 47-46 victory over France, confirming a spot in the Second Round at the U18 European Championship Women.

Please subscribe to our YouTube channel (http://www.youtube.com/user/FIBAworld?sub_confirmation=1)  
Follow the event on the official Championship page: u18women.fibaeurope.com 
Follow this summer's FIBA Europe Youth events on social media:
Youtube: https://www.youtube.com/fibaworld
Facebook: https://www.facebook.com/FIBAEurope   
Twitter: https://twitter.com/FIBAEurope