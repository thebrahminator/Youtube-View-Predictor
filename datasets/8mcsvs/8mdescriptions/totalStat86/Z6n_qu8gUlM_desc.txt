Andecy by Andrew York, played by me, Samantha Muir.  I have always loved the relaxed, easy flow of this piece.  I know my YouTube friend Sabine likes Andrew York's music, so this is for Sabine.  Hope you all like it too!   Photos by my friend Josie Elias.  I don't know what significance the pig has (he is a Vietnamese Pot-Bellied Pig), but I thought he has a nice smile :-)

For more information about me, or to buy my CD go to
http://www.samanthamuir.com/Samantha_Muir/Home.html