taken from EREB ALTOR - "Nattramn" (2015) CD/LP/Digital
Release-Date: 2015/04/24 via Cyclone Empire

Order here:
CD: http://www.metal-recycler.de/html/shop?viewitem=87100
Ltd.LP - blue vinyl: http://www.metal-recycler.de/html/shop?viewitem=87102
Ltd.LP black vinyl: http://www.metal-recycler.de/html/shop?viewitem=87101
Digital:
https://itunes.apple.com/album/nattramn/id984000482?l=de&ls=1
http://www.amazon.de/Nattramn-Ereb-Altor/dp/B00VVUJL2G/ref=sr_1_1?s=dmusic&ie=UTF8&qid=1429693850&sr=1-1&keywords=ereb+altor

https://www.facebook.com/ErebAltorOfficial

http://www.cyclone-empire.com
http://facebook.com/CycloneEmpire
http://www.twitter.com/CycloneEmpire 
http://www.soundcloud.com/cycloneempire
http://www.cycloneempire.bandcamp.com