View the underwater world of fish in the Cut River and underwater life, underwater fun with gopro fish videos with music and the love of underwater fishing camera action, a underwater adventure  of fish in the Cut river Houghton Lake Michigan, fish in the Cut river include Large mouth bass, Small mouth bass and Rock bass with the occasional northern pike. schools of small Redhorse suckers and minnows are also in good numbers. Not a lot of fish species but factor in the clear moving water over the sand bottom with shallow to deep landscape and good variety of underwater plants, roots and logs, its quite the site. Above water scenery has a little bit of everything from bog to cedar swamp banks, this video was taken late summer when the algae was reaching its peak.
 One could kayak the Cut river from Higgins lake to Houghton lake and only see a couple seasonal cabins, with this stretch of river the chance of seeing wildlife is good, I have seen Bear sign often, Eagles, kingfishers, herons and cranes, waterfowl and other water birds, turtles, snakes, muskrats and beaver. Last year we had to cross over a beaver dam at about a two foot water drop.
 Snorkeling the Cut river is my favorite but can be a challenge, snorkeling late summer the water is clear but the water table is low so some areas upstream will be snorkeled by foot, with limited access to the river snorkeling with the kayak is a must unless you know the few access trails, bloodsuckers also can be a problem the further down stream you go.

                                     View other underwater videos from snowsic.

* Fish Michigan. View underwater world of fish. Compilation 2012 2013 underwater fishing.
 *Underwater view of fish spearfishing bowfishing, underwater river fishing video, GoPro camera action
 * Underwater view of fish life at Tippy Dam, underwater river fishing video, GoPro camera action.
 * Underwater view of fish life in Dave's pond, underwater pond fishing video, GoPro camera action.                             
 * Underwater view of fish life Muskegon river, underwater river Pike fishing video GoPro camera action.
 * Underwater view of fish life at Reedsburg dam 2013, underwater river fishing video, GoPro camera action.
 * Underwater view of fish life, Longear sunfish underwater river fishing video, GoPro camera action.
 * Underwater view of fish turtle life in Dave's pond turtle underwater pond video, GoPro camera action.
 * Underwater view of fish life Reedsburg dam 2012, underwater river fishing video GoPro camera action.
 * Underwater view of fish life Houghton Lake, underwater backwaters fishing video, GoPro camera action.
 * Broad head arrow found in deer lung, hunting videos opening day fire arm deer hunting season.
 * Startup aquarium Cichlid video, home aquarium setup, home show tank of juvenile African Cichlid.