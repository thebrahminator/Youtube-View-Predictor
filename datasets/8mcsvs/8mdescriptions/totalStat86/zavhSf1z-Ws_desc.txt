Heyy!

Lenses that I'm wearing by Pinky Paradise - G&G King Size Circle Grey http://www.pinkyparadise.com/G-G-King-Size-Circle-Grey-p/c3-barbiegry.htm

Website & blog http://kikiidabest.com/
Facebook: https://www.facebook.com/kikiidabest
Instagram: http://instagram.com/kikiidabest
Beautylish: http://www.beautylish.com/kikiidabest

This makeup look ir natural soft crease with bright orange lips that  are 2014 spring/summer trend. It is perfect for everyday - school, work, etc but if you're used to not wearing makeup then it sure will be a nice look for foing out too. Add or skip false lashes, however you like!

Eyes:
Lorac Behind the scenes primer
Inglot freedom system palette
Lorac Pro palette
Maybelline colorama eyeliner in black
L'oreal Super Liner Carbon gloss
Elf eyebrow kit - ash
Maybelline Rocket Volum mascara

Lips:
MUFE Aqua Lip 1c
L'oreal Intense blondes 373
Maybelline 460 Electric Orange

Face:
MUFE HD Primer
MUFE HD Foundation
MUFE Concealer palette
L'oreal Touche Magique (warm beige)
MAC loose powder NW25
Elf blush
MAC Blush - desert rose
MUFE Sculpting kit
Disclaimer. All products shown in this video were purchased by me except for lenses that I got sent by PinkyParadise.