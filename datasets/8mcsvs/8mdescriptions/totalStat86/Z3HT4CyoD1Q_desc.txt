Attention: *Please read first*

This video is a run on expert drums:

For higher quality video add: &fmt=6 or &fmt=18 to the end of the URL link or click the link under the video watch in high quality, for HD quality, click the link under video "watch in HD"

If the video lags in HD, your computer needs more memory or is older.....try using the high quality or normal quality before saying its out of sync or lags xD I wish HD worked for every
one but thats not the case, sorry :(

Enjoy! :)

The drumset I'm using is the Goodwoodmods Stealth Mesh head drums. They are sturdy, VERY quiet and extremely responsive.....even the lightest tap will register a note. To find out more info go to http://goodwoodmods.com/products/inde...

Note: Platinum icon is for beating the endless setlist on expert, gold is on hard, and medium is inverse colors.

For those that keep asking, I jam on the Xbox360, my gamer tag is Jarr3tt88 :) (Note: Please don't send me a friend request UNLESS you have at least 55% of the DLC, variety is fun!, thanks)

Check out this page every week for the new Rock Band DLC, as I will be posting new videos if I purchase the new DLC.

These videos are not meant to be elite, but rather to show the general public what the note charts look like and possibly how the covers of the songs sound, if applicable. I'm not the best drummer out there considering I've never played drums in my life, but I think I'm decent enough after a few months of game play; nor the best guitarist. I'll try my best to obtain 5 stars, but some songs will be difficult. If such a case arrives, I'll bring the expert chart into practice mode if I can't get a good run. Either way I will post the best run I can get.

Also, please leave the negative comments at the door. I know I'm not great, I'm not trying to do elite videos as most of them will be first, second or third runs. Enjoy the videos, and be able to make a decision about purchasing said DLC.


I'm using a Hauppauge 1212 HD PVR to record, Google for more info.