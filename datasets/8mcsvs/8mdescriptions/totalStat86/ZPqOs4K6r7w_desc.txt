Bruce Zaccagnino made millions in software. He has invested much of that fortune into the construction of Northlandz, the world's largest model railroad, inside his home north of Philadelphia. Bruce gave us a private behind-the-scenes tour.

For more information on Northlandz, visit: http://www.northlandz.com