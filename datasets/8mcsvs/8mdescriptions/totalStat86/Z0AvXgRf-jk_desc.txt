Welcome to sour train! On this show members of the we crew take 3 different sour products and pass them around performing a sour train. Today Matt Zion & Chris Wreckless try Skittles Sour, Warheads Sour Dippers, & Cry Baby Extra Sour Supercharged Chewy Candy!

Buy wreckless eating shirts here - http://wrecklesseating.spreadshirt.com/
---
Follow us on twitter -  http://www.twitter.com/wrecklesseating
---
Check out our website - http://www.wrecklesseating.com/
---
Join our facebook fan page - https://www.facebook.com/WrecklessEating
---
Credit For Music - Title: Cold Funk By Kevin MacLeod (http://incompetech.com/m/c/royalty-free/) Licensed Under Creative Commons "Attribution 3.0" http://creativecommons.org/licenses/by/3.0/