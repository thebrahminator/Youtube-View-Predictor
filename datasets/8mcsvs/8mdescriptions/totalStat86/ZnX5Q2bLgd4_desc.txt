Teddybears remix of Lisa Miskovsky's song Still Alive from the video game Mirror's Edge.

Mirror's Edge is a free running (parkour) platform game for Xbox 360, Playstation 3 and PC.  The game is developed by Dice and published by EA.

You can find the original version of Still Alive here:
http://www.youtube.com/watch?v=S3CNfBGRUrk

You can find the Paul van Dyk remix of Still Alive here:
http://www.youtube.com/watch?v=DqssYoEaL7I&feature=related

You can find Shine, Alcorus' very popular and quite brilliant instrumental take on Still Alive here:
http://www.youtube.com/watch?v=wp1WY2ldjxE&feature=related

You can find Alcorus' excellent follow-up track to Shine here:
http://www.youtube.com/watch?v=72AlgHOsi_g

Music set to my own edit of Mirror's Edge footage.