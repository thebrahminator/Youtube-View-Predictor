Laugh and learn!

The brilliant Robert Newman comes to grips with the wars and politics of the last hundred years - but rather than adhering to the history we were fed at school, places oil at the center stage of all the cause of all commotion.