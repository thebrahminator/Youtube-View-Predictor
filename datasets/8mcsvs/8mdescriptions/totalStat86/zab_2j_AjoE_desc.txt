To order a copy of this video, please visit http://www.bennettmarine.com/fishing_skills.html

A veteran Captain shares his Grouper & Snapper secrets.
Grouper and Snapper are not hard fish to catch if you know what to do. In this informative program, Captain Frank teaches the techniques to make your fishing better, more fun and provides the tips to catching more fish - the easy way.

With GPS great fishing locations are located and with the help of electronics the fish finder locates where the fish are. Watch Captain Frank bait up and see the action start.

If you want to fill your cooler with Snapper and Grouper - this is the program for you!

Tips & Techniques Include:
Catching & using live bait.
Choosing your location.
How to find fishing spots.
Using your electronics.
Chumming.
Rod & reel selection & set up.
Lures & line.
Hooks (Circle Hooks).
Live bait.
Artificial bait.
Trolling.30 min.