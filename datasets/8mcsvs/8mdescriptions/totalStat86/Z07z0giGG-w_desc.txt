Eelke Kleijn proudly presents the official video for the Original Mix of Ein Tag Am Strand! Grab your copy on iTunes here: http://smarturl.it/eintagamstrand

Subscribe to Spinnin' TV here: http://bit.ly/SPINNINTV

Rotterdam based Eelke Kleijn proudly presents his first release on Spinnin'. 'Ein Tag Am Strand' (German for: a day at the beach) is a wonderful and distinct summer tune. Think white sandy beaches, tropical cocktails and late night camp fire parties. What else could you ask for?

Pete Tong / BBCR1 : 'Played in my Essential Selection on BBC Radio 1'
Danny Howard / BBCR1 : 'Played in my radioshow on BBC!
Sander van Doorn : 'Support'
Dimitri Vegas & Like Mike : 'Played in our radioshow'
Martin Solveig : 'Full summer vibe!'
Fatboy slim : 'You have my attention! very cool, refreshing and different!'
Rudimental : 'Now this is our new favorite! Absolutely banging! the search for plugins truly does pay off!'
Sander Kleinenberg : 'Supporting!'
Tocadisco : 'Original'
Hernan Cattaneo : 'Support'
Fake Blood : 'Liking this!'
Paolo Mojo : 'This isn't what i was expecting! Im not sure what to m ake of it, but i like it!'
Moonbootica : 'Loving this!'
Aeroplane : 'love it!
Sister Bliss / Faithless : 'very simple and warm!'

Follow Eelke Kleijn:
http://eelkekleijn.com
http://facebook.com/eelkekleijn
http://twitter.com/eelkekleijn
http://soundcloud.com/eelke-kleijn
http://youtube.com/eelkekleijn

Management
epiqurus.com

Follow Tres:Or on:
Twitter: http://twitter.com/Tresor_music
Facebook: http://bit.ly/Tres_Or

Directed by Danny Merk, KGBAmsterdam.com

Thanx to all volunteers & locations in Ibiza!