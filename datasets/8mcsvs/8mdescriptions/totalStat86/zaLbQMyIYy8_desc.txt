Learn about details of flat picking a guitar in this free pedal steel guitar video.

Expert: Aaron Moons
Bio: I received a BA in Communications/Media studies, played in the jazz ensemble and combo. Since living in Chicago I have played drums for several successful bands.
Filmmaker: Christian Munoz-Donoso