Valve recently released a top-down shooter called Alien Swarm based on the steam engine. The game was released free with the source code and could be downloaded from here.  http://store.steampowered.com/app/630/

Shrimpsess, on Steam forums recently provided console commands for the game, and included within those were commands to turn the game into a 1st person shooter mode, this is only available while playing in single player mode. 

Enter the following commands into the developer console to enable 1st person view (fps), enjoy: 

firstperson
asw_hide_marine 1
asw_controls 0

For more commands see the original post : http://forums.steampowered.com/forums/showthread.php?t=1371124