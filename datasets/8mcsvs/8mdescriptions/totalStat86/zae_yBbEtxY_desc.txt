Results of week SEVENTY-SIXs OCE editing contest!

Hey guys! Hope you enjoy this weeks results. Thanks to everyone who got involved, be sure to check out week 77 ;)
As always feel free to share your opinions but please do so with respect to OCCs judges and contestants. Also, feel free to message me if you want feedback or if you just want to ask me a general question about the results. I'll get back to ya asap ;)
- Cuttz

Week 77: http://www.youtube.com/watch?v=iBJybmh7_tY&hd=1
Commentator: http://www.youtube.com/user/Geoffology

Verum: http://www.youtube.com/VerumEditing
Pin OCC!: http://www.youtube.com/subscription_manager

1st Place: byAltiin ; http://www.youtube.com/watch?v=Kg0V7EEurbY&hd=1
2nd Place: PidgyEdits ; http://www.youtube.com/watch?v=SGDd2j4Gojk&hd=1
3rd Place: xSnaKz ; http://www.youtube.com/watch?v=5R4QsnnKK5I&hd=1
4th Place: freshpoutine ; http://www.youtube.com/watch?v=wgOM-8Lkwkg&hd=1
5th Place: ImCheddah ; http://www.youtube.com/watch?v=EwVgsYimXkM&hd=1

Honourable mentions in no particular order (people who could have got in the top 5 but didn't quite make it this time): 
[PulleyEdits] http://www.youtube.com/watch?v=AmHAjYodaVE&hd=1
[ximCUBE] http://www.youtube.com/watch?v=4ToJLCBujJc&hd=1
[ArvenEdits] http://www.youtube.com/watch?v=LNmpnBwBpcc&hd=1
[TehMistHD] http://www.youtube.com/watch?v=jN5XeYAIgW4&hd=1
[xiRadin] http://www.youtube.com/watch?v=cOrRi2mP1fU&hd=1
[ByYeeqs] http://www.youtube.com/watch?v=OZf15XnikVM&hd=1
[Uniquur] http://www.youtube.com/watch?v=DDOByxfdptw&hd=1
[SickEditsCZ] http://www.youtube.com/watch?v=4Lj8G-_Gx5k&hd=1
[iGuaRdexX] http://www.youtube.com/watch?v=zMQHYd5teEg&hd=1
[rngerfire07] http://www.youtube.com/watch?v=bsbHINyJM7A&hd=1
[bearCNY] http://www.youtube.com/watch?v=CoKplyc6yhU&hd=1
[DeathbyCookiewon] http://www.youtube.com/watch?v=fhM_Fb-wmNE&hd=1
[Mas21Edits] http://www.youtube.com/watch?v=7D4sSgO2ew4&hd=1
[fidorOo] http://www.youtube.com/watch?v=ow93XA4kBhY&hd=1
[ScHooLZzHD] http://www.youtube.com/watch?v=1u41kiEnmis&hd=1
[JaintedHoe] http://www.youtube.com/watch?v=Y7tzAzJ1l7s&hd=1
[RagnarokEdits] http://www.youtube.com/watch?v=QWF2oqg4Msc&hd=1
[AzoEditing] http://www.youtube.com/watch?v=uMeauv_IwbI&hd=1
[j0wsh] http://www.youtube.com/watch?v=w2gxJkflb6g&hd=1
[xGarrettful] http://www.youtube.com/watch?v=zWRQjVgOD8g&hd=1
[VodkaEdit] http://www.youtube.com/watch?v=gFNrOpdtjXU&hd=1
[NightCallNiga] http://www.youtube.com/watch?v=foQ00IBHBmY&hd=1
[CroHack97] http://www.youtube.com/watch?v=870bwtxSzk0&hd=1

When we are judging the OCEs were are looking for a few main things that make a good edit. The things we look for are:
Originality: You should always try to stand out from the crowd and do something a bit original. Originality can be shown by using unknown songs, un-seen effects or maybe even an original theme!
Creativity: As i said with originality, you should always try to stand out from the crowd and a good way to do this is to show off your creative side! There are lots of things you can do with an edit, get creative!
Execution: Execution basically means how well the edit was done. Was we can tell if the edit is well executed:
           - Is the edit well synced?
           - Does the edit flow well?
           - Is the cc well made and fitting for the edit?
           - Are the effects used well implimented in the edit?
           - If transitions are used, do they look good and keep the edit flowing?
Advanced editing: Another good way to make your edit stand out from everyone elses is to used some advanced skills. For example:
           - Motion tracking can be used to break up the edit or make a nice introduction to an edit
           - Cinematics can be used to break up an edit and help keep the edit flowing nicely!

NOTE that this next bit is for players:
If you want your in game clip featured in next weeks contest then:
- Make sure its HD, 59.94 fps and disabled resampling
- Make sure its Beast!
- Send us a message titled *Clip request* and upload the clip unlisted on youtube or to a download site 

Thanks you for watching. Please make sure to leave feedback and rate thumbs up to help us out! :)

- Cuttz