Durham Academy's Cavalier Concert Chorus performs a medley of songs from The Lion King.  While not a perfect performance:

1. We went sharp during the opening of Circle of Life;
2. Some issues with step-touching;
3. A computer glitch that forced us to go back and restart at the end of Can You Feel the Love Tonight

... I thought it went particularly well.  From our evening Spring Concert, April 29th, 2010.

Part 2 includes:
1. Shadowland (Kelsey Brian, solo)
2. Endless Night (Shan Nagar, solo)
3. Can You Feel the Love Tonight (Kelsey and Shan)
4. King of Pride Rock
5. Circle of Life (Reprise)