This is my Live Fire series: it's a complement to my Target Practice videos. This series focuses more on demoing the weapons in live combat, relative weapon damage at baseline, and accuracy from cover.

The Paladin is purchased from Spectre Requisitions either on the Citadel in the Spectre Office or on the Normandy. Its cost ranges from 200k-220k credits.

My Target Practice series can be found here: 

http://www.youtube.com/playlist?list=PLD522CE4888829B19&feature=plcp

The full Live Fire playlist can be found here: 

http://www.youtube.com/playlist?list=PL1C2F8061B1E222CE&feature=edit_ok