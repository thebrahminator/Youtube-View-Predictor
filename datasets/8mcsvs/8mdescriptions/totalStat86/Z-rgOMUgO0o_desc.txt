Somos um grupo de residentes do Second Life que tem por
objetivo fazer do metaverso um lugar mais agradvel. Seguimos a 
temtica dos LANTERNAS VERDES, personagens heroicos que protegem o 
universo.
Somos um grupo ativo pronto para auxiliar aos residente, 
atuamos tambm como:

ORGANIZAO ANTI -GRIEFER:

Nossa principal ferramenta  o dilogo.
Usamos nossos conhecimentos em  conjuno com o sistema de Report Abuse da Linden Lab, sendo este utilizado em ltimo caso . 
Ns no utilizamos qualquer mtodo  ofensivo. 
Ns no violamos os Termos de Servio da comunidade. 
Ns seguimos um cdigo de tica que amplia ainda mais as normas 
comunitrias. 

ORIENTAO E AUXILIO:

Orientamos residentes nas mais diversas situaes. 
Atuamos em locais e situaes procurando estabelecer a harmonia.
Patrulhamos lands.
Realizamos  palestras relacionadas aos Lanternas Verdes no Second Life.

O QUE  UM LANTERNA 
VERDE ?

Dos incontveis mundos do metaverso tecidos pela imaginao de cada residente, nasceram os Lanternas Verdes. Das mentes dos Guardies surgiu a centelha esmeralda a moldar um residente atuante,
sempre prestativo, de comportamento polido, disciplinado em seus 
deveres, pronto a auxiliar a qualquer que solicite ou necessite. Surgiu,
enfim, o primeiro Lanterna Verde na imaginao dos Guardies e este foi
molde de todos os demais. Um extremo desejo de ajudar e servir norteia a
vida do Lanterna Verde onde quer que ele esteja e a qualquer momento. 
Sua misso  nobre e desprovida de recompensas materiais, sua satisfao
 a evoluo pessoal atravs do refinamento de suas qualidades rumo  
excelncia. A ordem  onde vive, e onde quer que ela no exista ele 
l estar para traz-la. No so guerreiros, mas tem o esprito 
aguerrido; no usam ofensivas, mas defendem e fazem cumprir as leis do 
metaverso; no so possuidores de nada, mas sabem que estes muitos 
versos so de todos.

No limiar da morte disse: 

"A lanterna  a fonte.
O caos  conquistado.
A ordem  mantida.
Empunhe o poder.
Mantenha a ordem.
 uma grande honra.
O anel e a bateria so
Tudo de que voc precisar" 

Abin Sur - Amanhecer Esmeralda 

*************************


Temos reunies semanais em nossa base.
Se voc tiver alguma dvida ou sugesto:

Visite-nos em:
http://slurl.com/secondlife/rust%20spot/171/157/3389/


ou
acesse nosso frum:
http://lanternasverdesbr.forumeiro.org/

Participe de nossa comunidade no orkut:

http://www.orkut.com.br/Main#Community?cmm=54440389


Visite nosso site:  

http://lanternasverdes.xp3.biz/


"No
dia mais claro
Na noite mais escura
Nenhum mal escapar a minha viso
E aqueles que cultuam o mal
Temam o meu poder
A luz do Lanterna Verde!"

GRUPO inworld: Lanternas Verdes