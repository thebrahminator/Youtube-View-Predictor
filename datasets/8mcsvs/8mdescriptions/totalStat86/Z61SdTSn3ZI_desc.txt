What a joy to perform in the ancient open theater "Dora Stratou", surrounded by Acropolis and sky full of stars in lovely Athens (Greece), in TOTAL ORIENTAL gala show, featuring Saida Helou and Yamil Annum! Organised by Prince Kayammer and Yasmin Kalathaki! July 2014!
Music: Wahastiny- Nour Mhanna
Improvisation/Choreo: Leila
If you wish to book Leila to teach/perform in your event, you can contact her via www.leilaoriental.com