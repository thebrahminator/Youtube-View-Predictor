Catapult Trick Shots: https://youtu.be/xwPHRciTBGk

Here's some behind the scenes footage of the "Slap Shot" from our Catapult Trick Shots video. Let us know what shot you want to see some behind the scenes of next!

Want to keep up with us on social media?
https://www.youtube.com/dcustudios2
http://instagram.com/LegendaryShots
https://twitter.com/LegendaryShots
https://vine.co/LegendaryShots

Like our shirts? You can get them here!
http://legendaryshots.spreadshirt.com/

For business or media inquiries: thelegendaryshots@yahoo.com
For online use, you may embed this YouTube video only. No other embedded players are permissible.