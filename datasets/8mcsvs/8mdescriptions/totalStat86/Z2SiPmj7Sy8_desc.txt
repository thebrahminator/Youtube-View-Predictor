Stream Link: http://www.twitch.tv/xionikandsheyrah
Website: http://xionikandsheyrah.wix.com/gaming
Support Xionik & Sheyrah (Donate Link): http://xionikandsheyrah.wix.com/gaming#!untitled/cdx

===================================

This is an in depth video covering most aspects of tailoring and how to make gold with this profession. Some of the topics covered are crafted gear, leg armors, bags, shirts and how to make large quantities of cheap vision dust very easily. I've also indicated throughout the video the best times to sell certain items and why.

Here are direct links to the diagrams I made for this guide.

Tailoring Crafted Epics:
http://xionikandsheyrah.wix.com/gaming#!Tailoring-Crafted-Epics/zoom/c14ep/image1gok

Tailoring Crafted Malevolent:
http://xionikandsheyrah.wix.com/gaming#!Tailoring-Crafted-Malevolent-Gear/zoom/c14ep/imageo8b

Tailoring Blue Procs:
http://xionikandsheyrah.wix.com/gaming#!Tailoring-Blue-Procs/zoom/c14ep/image4j5

Tailoring Leg Armors: 
http://xionikandsheyrah.wix.com/gaming#!Tailoring-Leg-Armors/zoom/c14ep/imagerth

Tailoring Bags: 
http://xionikandsheyrah.wix.com/gaming#!Tailoring-Bags/zoom/c14ep/imageymq

Tailoring Shirts: 
http://xionikandsheyrah.wix.com/gaming#!Tailoring-Shirts/zoom/c14ep/image1klr

Vision Dust:
http://xionikandsheyrah.wix.com/gaming#!Vision-Dust/zoom/c14ep/image1igz