Matteo Ruggiero esegue una serie di licks e fraseggi nello stile di Steve Vai. Solo tratto dal video Steve Vai quick Licks, Licklibrary

Matteo Ruggiero plays some Licks in the style of Steve Vai, from Licklibrary "Steve Vai Quick Licks", by Andy James.
Gear:
Achia Universe with Mama Texfire (b) and Supernatural (n) pickups, Reference Cables.
If you like it, join my Facebook page:
https://www.facebook.com/pages/Matteo-Ruggiero/183896598451964?fref=ts