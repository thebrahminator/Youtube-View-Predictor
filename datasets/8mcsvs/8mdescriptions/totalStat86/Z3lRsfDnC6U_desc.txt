POLLO AROMATIZADO AL LIMN por Cocinero Diario

Antes de nada suscrbete  ES GRATIS!!! http://www.youtube.com/user/MrZexions?sub_confirmation=1

Si tienes alguna duda o quieres aprender mas recetas visita mi web:
www.cocinerodiario.com
Tambin podis seguirme en:
Facebook: https://www.facebook.com/cocinero.diario
Twitter: https://twitter.com/Cocinero_Diario
Google+: https://plus.google.com/+CocineroDiario/
Instagram: https://instagram.com/cocinero_diario/

Para 2 personas
Preparacin: 15 min
Coccin: 15 min
Ingredientes: 4 filetes finos de pechuga de pollo
                        organo                    
                        2 raciones de espaguetis
                        perejil
                        el zumo de 1 limn
                        aceite de oliva 
                        sal       
                        pimienta

Procedimiento:Con ayuda de un rallador ralle la corteza del limn y resrvela. Corte 4 rodajas gruesas del limn y el resto crtelo en finas rodajas. Salpimiente las pechugas de pollo y eche las rodajas finas de limn junto con el organo solamente en dos de los filetes. Ahora montaremos los filetes restantes sobre los otros 2 filetes a modo de sndwich. En una cacerola con agua hirviendo, echaremos un puado de sal y pondremos a cocer los espaguetis. En una sartn con aceite caliente, pondremos las rodajas gruesas de limn y encima de estas los filetes de pechuga de pollo que hemos preparado previamente. Una vez que vemos que el pollo esta hecho por un lado le daremos la vuelta con cuidado de dejar las rodajas de limn sobre la sartn, para volver a colocar las pechugas encima de estas. Una vez cocidos los espaguetis, los escurrimos y los echamos en una ensaladera. Ahora los aliaremos con sal, pimienta, la ralladura del limn y el zumo de otro, perejil picado y aceite de oliva, mezclndolo todo muy bien. Sirva los espaguetis en un plato junto con el pollo

Si os ha gustado esta receta darle a LIKE y no os olvidis de comentar que me encantan todos vuestros comentarios.