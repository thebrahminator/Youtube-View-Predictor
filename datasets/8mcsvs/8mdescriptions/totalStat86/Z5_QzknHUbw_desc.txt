www.unitriders.com

Unit Clothing and Crusty Demons joined forces to organize the biggest film shoot in Australia for 2010. Matt Schubring hosted the incredible heli shoot at his private compound in Boonah, where he has built a stack of amazing natural terrain jumps and tracks. 

X Games Gold Medallist Josh Hansen joined the action, along with Beau Bamburg, Robbie Marshall and Todd Waters. 

Courtesy: Unit Clothing 

Music: 'Past Lives' by The Bronx www.thebronxxx.com 

Producer: Allan Hardy 

Additional Footage: Jon Freeman + Jason Macalpine