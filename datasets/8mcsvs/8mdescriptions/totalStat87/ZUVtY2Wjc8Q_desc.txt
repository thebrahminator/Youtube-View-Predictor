Discover Toyotas next big thing, the C-HR Concept as it makes its world premiere at the Paris Motor Show 2014. Explore every eye-catching detail of the new compact crossover, from the coupe roof line and floating rear lights, to its muscular stance and diamond architecture styling.

Toyota Europe website: http://www.toyota-europe.com/
Toyota Europe Newsroom: http://newsroom.toyota.eu/
Toyota Europe Blog: http://blog.toyota.eu/
Toyota Europe on Twitter: http://www.twitter.com/toyota_europe/