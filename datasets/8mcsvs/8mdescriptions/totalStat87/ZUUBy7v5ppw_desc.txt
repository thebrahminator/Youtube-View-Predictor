Hey everyone! Some of the fake Monster High dolls I bought got makeovers! I hope you guys like them. Part 2 will be up sometime this week! Monster High rules!

Song: "Glowing"

July: July loves to travel and explore! Since she was a little ghoul, she has been fascinated by the world around her. She plans to travel to as many places as she can within her life.

Emma (aka Crash): Emma is an interesting ghoul. She loves to roller skate! She lives for skating stunts, but she also enjoys crashing into other skaters! Her friends all call her crash. Her hair is always a messy because she skates very fast. She doesn't seem to mind.

Glitch: Glitch is a computer bug, a monster to computers! Glitch is quite a prankster. She likes hacking people's account on dating sites and making things super awkward. It always gives her a good laugh! She once tried to hack the computers at monster high as an April fools prank, but she got caught by a teacher! 

Kitty: Kitty is a talented gymnast. Being a were-cat, she is very good at balancing, is super flexible, and always lands on her feet! She's fearless, because she has nine lives. She also teaches a class after school for little kids. They love to learn and she loves to teach them. Each class, a kid usually asks her to do a cool trick. She always does and they think it's so cool!