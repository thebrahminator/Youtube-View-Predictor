http://www.gleanergames.com/pc/games/adventure/mystery-trackers-void-collectors-edition.html

The Void family mansion used to be a gorgeous home, until Dr. Malleus Void inherited it. Legend has it that the creepy doctor spent his time doing terrible experiments in the home, and it has been abandoned for an entire decade. After 3 world famous celebrities go missing inside the house, it's up to the Mystery Trackers to go inside the house, and solve its mysteries once and for all in Mystery Trackers: The Void! This is a special Collector's Edition release full of exclusive extras you won't find in the standard version. As a bonus, Collector's Edition purchases count toward three stamps on your Monthly Game Club Punch Card! The Collector's Edition includes: 

Additional gameplay 
Original soundtrack 
Built in Strategy Guide
Concept art