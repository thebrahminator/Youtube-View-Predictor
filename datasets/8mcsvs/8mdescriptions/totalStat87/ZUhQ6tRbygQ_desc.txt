Advertising in the Times Record print edition has many benefits, not the least of which is that your ad appears on the Times Record website, in quite a few places! Let's count them.

Display Ad Rotation
One: Your ad gets the most website exposure by appearing in the rotation of ads on the right hand column of every page on the website. The placement of ads is completely random, ensuring equal market reach to all advertisers. Also, we display the same high resolution graphics used in print.

Advertiser Categories
Two: On the top of every page, directly below the buttons to the various news sections is the advertiser navigation bar, which provides links to the advertiser categories, where all ads of the same category appear together.

Advertiser Index
Three: Your contact information appears in the advertiser index which is a collection of all advertisers who have placed an ad in the Times Record at least once in the recent past.
At the top we have a map displaying all the advertisers, each represented selectable by pins, and directly below is the index.

Profile
Four: Each advertiser featured on the website is given a customizable advertiser profile. This section includes the same general information on the business shown in the index, as well as a display of all currently running ads in the paper, and a QR code that links to this profile page. Readers can access your profile by clicking on the ad itself from any point in the website.

PDF Versions
And,  Five, "Your ad also appears in several PDF versions of the paper, including a basic PDF viewer, Page View, and an interesting application called flip book. 

Control Center
You also have access to an advertiser control section with an administrative login, which enables you to "edit" your business profile, or add other "general information" [add "Hours 8 am to 9 pm")
 The advertiser control section also features valuable metrics on your advertising campaigns, such as click through rate and profile impressions, over a period of time.

But you don't have to do anything with the advertiser profile, by placing an ad in the print edition, your business automatically appears in five different places on the Times Record Website: in the right hand column, advertiser category, the Advertiser Index, your unique profile, and several PDF versions.  

All this happens by simply placing a print ad in the Times Record.