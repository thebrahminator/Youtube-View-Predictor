 http://clashtournaments.com
Check out our website for more videos and articles about Super Smash Bros., Pokemon, and more!
 http://clashtournaments.gg
Check out and cheer on our professional gaming team here!

Be sure to Comment and Like the video if you enjoyed it! Give our channel a follow for all the great content we put up!

The set this event was from was streamed and recorded live by CLASH Tournaments. You can watch our live streams on the following channels:
 http://twitch.tv/clashtournaments
 http://mlg.tv/clashtournaments

Find us on our website and social media networks:
 http://clashtournaments.com
 http://facebook.com/clashtournaments
 http://youtube.com/clashtournaments
 http://twitter.com/clashtournament
 http://instagram.com/clashtournaments
 http://twitch.tv/clashtournaments
 http://mlg.tv/clashtournaments

Be sure to Follow and Subscribe to us!

CLASH Tournaments provides content for many titles, with a specialty in the Super Smash Bros. series of games. Check us out for content on:
Super Smash Bros. (N64)
Super Smash Bros. Melee (GCN)
Super Smash Bros. Brawl (Wii)
Project M (Wii)
Super Smash Bros. for 3DS / Wii U (3DS / Wii U)
Pokemon X & Y (3DS)
Pokemon Trading Card Game
and More!