ReMixer: Kaijin
http://ocremix.org/remix/OCR00892/
FREE at http://ocremix.org  SHIRTS & HOODIES! http://ocremix.org/store/  DONATE! http://bit.ly/ocrPayPal
Facebook! http://facebook.com/ocremix  Twitter! http://twitter.com/ocremix

 Game: Radical Dreamers: Nusumenai Houseki (Square, 1996, SNES)
 ReMixer(s): Kaijin
 Composer(s): Yasunori Mitsuda
 Song(s): 'Ending ~ Le Tresor Interdit'
 Posted: 2002-12-31, evaluated by djpretzel 

Founded in 1999, OverClocked ReMix is an organization dedicated to the appreciation, preservation, and interpretation of video game music. Its primary focus is http://ocremix.org, a website featuring thousands of free fan arrangements, information on game music and composers, resources for aspiring artists, and a thriving community of video game music fans.

For media inquiries (interviews, articles, conventions) or soundtrack development with OverClocked ReMix, please contact us!
http://ocremix.org/info/About_Us

Video by Jos the Bronx Rican (Jos E. Felix)
http://www.bronxrican.com
Additional direction by Liontamer (Larry Oji)

OverClocked ReMix is a not-for-profit site that provides a ton of free music, which requires a ton of bandwidth and a pretty hefty web server. We need your help to keep the site running!
 http://ocremix.org/store/
 http://bit.ly/ocrPayPal

JOIN US:

 http://ocremix.org/forums/
 http://facebook.com/ocremix
 http://twitter.com/ocremix
 http://last.fm/group/OverClocked+ReMix
 http://youtube.com/group/ocremix
 http://facebook.com/group.php?gid=2210239078
 http://groups.myspace.com/ocremix
 http://orkut.com/Community.aspx?cmm=5991
 http://fah-web.stanford.edu/cgi-bin/main.py?qtype=teampage&teamnum=48303