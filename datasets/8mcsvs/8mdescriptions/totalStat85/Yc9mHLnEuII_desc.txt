Selleria Royalty Sponsor Miss Italia
Equitazione foggia

11 Giugno 2010 - Foggia - P.zza XX Settembre
Prima Tappa delle selezioni regionali "Miss Italia 2010"
 
Il tour estivo di Miss Italia, il concorso di bellezza pi famoso ed imitato della nazione,  ripartito ieri sera da Foggia. Sul palco allestito in piazza xx settembre, infatti, si sono sfidate le aspiranti miss Foggia, titolo provinciale che permette di incamminarsi sul percorso che porta all'ambitissimo titolo di pi bella d'Italia. La serata  stata condotta da Angelica Gianfrate e ha visto come Madrina delle selezioni provinciali Maria Perrusi, Miss Italia in carica.

All'interno della serata la sfilata di moda "Selleria Royalty"