Introducing the all new VS Athletics Custom Performance Uniform. Sublimated...not screened.
Create a totally unique design for your team. The design and colors are actually embedded in the fabric, not printed on top of the fabric. Won't flake or peel, much longer lasting than screen printed uniforms.

For more info please visit: http://www.vsathletics.com/section.php?xSec=197&xPage=1