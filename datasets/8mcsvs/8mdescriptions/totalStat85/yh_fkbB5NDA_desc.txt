DEKASETTIMO - PAGINA UFFICIALE FACEBOOK : 
https://www.facebook.com/dekasettimo?ref=ts&fref=ts

DEKASETTIMO - "DISTANTE DA TUTTI" official video 

Regia & Montaggio : Antonino De Lellis 


.CIANURO PROD. 2015.


TESTO : 

Voglia di vivere , saltami addosso . 
Covo di vipere , siamo in un fosso .
Non se ne esce , mi rincresce ma devo crescere 
e questo proprio non mi riesce ! 
Solita storia tra apparire ed essere ,
andando avanti ho perso mille tessere ,
compresa la tua faccia , e chi la rintraccia ? 
Stupido io , pensavo desse benessere .
Piu' dai fiducia e piu' ti viene tradita ,
siamo dentro una gita ma senza spiriti guida . 
Chi me lo placa quest'istinto suicida ? 
Ho un sorriso finto com' finta la vita , 
quando ti ho dato la mano tu mi hai strappato le dita ! 
Davvero , bella partita .. 
Non e' finita in parita' , tu avevi un mitra , 
io mani vuote , tu hai fatto fuoco , poi sparita infine . 
Ti ricordi di me ? Sono quello a cui rubavi tempo , come Lupin ! 
Adesso siamo piu' o meno come due piume , 
saliamo leggeri come carpe in un fiume . 
Vetri fume' , 
non vedo piu' niente , non vedo piu' me . 
Passo e saluto , 
sono caduto in basso quindi passo e chiudo . 

Rit : 
Questo tempo vola , 
il cielo e' viola , 
magari si migliora . 
Qualcuno lo si ignora , 
questo tempo vola ,
il cielo e' viola , 
magari si migliora , 
qualcuno lo si ignora .
Qualcuno lo si ignora , 
la vita e' una signora pignola ,
fin troppo bene , sin'ora .. 
Ma nel tempo , credimi , qualcuno lo si ignora . 


Ho cancellato piu' di un nome in rubrica , 
madamigella , io sono un uomo all'antica ! Fidati ! 
Non dico mica "Chinati" ! 
Non sarebbe da me , qui e' rivolta : Tienamen . 
Ho cicatrici sulla schiena , ma chi me le ha fatte ? 
Troppe persone inadatte ,
hai fatto tanto , ora vattene . 
Esplodo d'odio che non so come trattenere . 
Tiro su difese , come un carpentiere , 
giro a piu' riprese , quindi sta a vedere ! 
Tu ridi a vedermi cadere ed a bere
il mio sangue da dentro un bicchiere . 
Devo andare veloce , 
come un cane feroce .
Come un cane randagio ,
tu sei infame e malvagio . 
Difficile pensare che tutto ritorna , 
qualcuno subentra , qualcuno si scorda . 
Non ci pensare che in fondo sei in forma , 
qui e' tutto una merda .. E scusa la forma ! 
Ci formano queste ferite luminose come pepite ,
ci abbiamo davvero sofferto , 
sono un gabbiano in questo cielo aperto ! 


Rit.