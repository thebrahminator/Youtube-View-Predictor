HD encoding test.

Due to the encoding of this video, it does NOT have to be watched in HD for excellent picture quality.

Needless to say, I am running behind with Red Queen...still filming actually, and I have other things going on in my life that are quite frankly more important than games at the moment, so delays are inevitable.

Anyways, I just wanted to see if I could play with my encoder on an old vid to see if I could get these old AVIs into HD, and sure enough, I was able to do it. The original Video of Pink Panther will remain up for those that want to watch in HQ.

Original Synopsis of this coaster:


My 2nd No Limits Coaster

Boy=Crazy Eights
Girl=Pink Panther

After I thought I had decent success with the "figure 8" wood coaster, I wanted to create a simple "out-n-back" wood coaster. Well after the 1st drop, that plan went out the window. It's half air-time, half twister. The 560 degree helix was inspired by the Crystal Beach Cyclone, though it is an element that closely resembles what you find on Son of Beast.

If you watched my first vid and read the synopsis, I built two coasters, one "boy" themed and the other "girl" themed.

I'm still learning, but this one is far smoother, yet more intense than Crazy Eights. And I'm still working on getting better.

things to note:

Max G of 4.9 was achieved at the bottom of the 2nd hill-strange, I thought that was smooth. It hit 4.6 @ 2:43 (a problem spot) and it hit 4.5 @2:57 (another problem spot).

It hit the max negative G's @ 3:14 coming out of the helix. The helix itself, carries a nice 3.5G vertical pull, really pleased with that; I was aiming for 3.8 to 4.0.

Now I like this coaster a lot, but 1 part pissed me off. At 2:57, there was a nasty bump. It affected the G's, and it frustrated me because I tried like 10 times to smooth it out, and it simply didn't want to cooperate.

Well at anyrate, I hope you enjoy. Comments are welcome