http://jsmugen.x10hosting.com/index.php?topic=6283.msg97365#msg97365
Event #190: Wrath of Freeza
Planet Namek is under attack. Freeza wants the Dragon Balls, but Vegeta has his own agenda. The Ginyu Force poses a threat, but lets take care of Vegeta first.

You: Your Choice
Opponents:
[*] Vegeta (Choujin)
[*] Burter (Stig87)
[*] Captain Ginyu (Stig87)
[*] Freeza (Choujin)
Goal: 4-on-1 Turns. Characters must be fought in order presented. Cannot be KO'ed once.
Stage: Anything DBZ related
BGM: Your Choice

Decided to try someone different this time, although anyone else could've gotten this done too, I suppose.  My brother has been a big fan of Nash(Or Charlie) ever since he and I first played Street Fighter Alpha 2 a long time ago.  I never liked charge characters when I was a kid because I was never sure if you had to charge for five seconds or not, and, sometimes, I failed at it, but I was a kid, so patience wasn't one of my strong points.  Anyway, I've always favored Nash to Guile though because, well, Nash LOOKS cooler, has awesome win poses, and how many people do you know who come back from the dead twice?

...Wait a minute.  I'm doing something DBZ and Nash related.  It's not uncomm-  *eats own words*

Nash was made by Phantom.of.the.Server., Burter and Ginyu were made by Stig87 and can be found here: http://mugendream.com/index.html

Vegeta and Frieza were made by Choujin.

The stage was made by Alien-san, and the music is the "Royal Guard" from Dragon Ball Z: Ultimate Battle 22.

Well, enjoy.