Photo montage from 2010's Emory University School of Medicine Commencement. The ceremonies took place May 10, 2010. Emory University School of Medicine is ranked among the nation's finest institutions for education, biomedical research, and patient care.

Related Links

Emory Commencement 2010
http://www.emory.edu/home/about/history/commencement.html

Emory University Commencement
http://www.emory.edu/commencement/

Emory University School of Medicine
http://www.med.emory.edu