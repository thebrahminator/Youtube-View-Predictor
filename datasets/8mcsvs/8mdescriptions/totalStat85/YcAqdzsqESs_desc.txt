Sewing Piping into a Seam. Piping is used within fashion sewing to give a decorative finish to our sewing projects. No matter which type you use, be it flat piping or cord, we insert it into an edge of a garment to create some wonderful eye catching designs.  Once we have sewn our piping into place, there are a few options open to us on how we can finish the look. We could edge stitch, topstitch or hand stitch the seam allowance on the wrong side.

SUBSCRIBE for more free fashion sewing tutorials -  https://goo.gl/sJzW6P

 
SUPPORT THE CHANNEL BY SHOPPING VIA MY AMAZON STORE FOR YOUR SEWING SUPPLIES 

SHOP FOR YOUR SEWING SUPPLIES HERE  -  http://amzn.to/2n1htvv

* Fabric scissors -  http://amzn.to/2nNocGz

* Sewing Thread -  http://amzn.to/2mDZ0nX 

* Sewing box - http://amzn.to/2nwtdmu


    
BEGINNER SEWING MACHINES  

Janome  - http://amzn.to/2oM41tl

Brother  -  http://amzn.to/2o5TScY

JUKI  - http://amzn.to/2nfdkFf


SEWING MACHINES  UPGRADE

Janome  - http://amzn.to/2nNr1HP

Brother  -  http://amzn.to/2oGTin5

JUKI  -  http://amzn.to/2nGYlPB  



CONNECT WITH ME  

Follow on INSTAGRAM
https://www.instagram.com/colleenglea/

Follow on PINTEREST
https://uk.pinterest.com/FashionSewing/

Tweet Me TWITTER
http://www.twitter.com/ColleenGLea/

Like on FACEBOOK 
http://www.facebook.com/pages/Sewing-istas-Gallery/



 
Don't forget to subscribe, hit the bell to receive notifications of when I upload videos and remember to comments it's always GREAT to here from you!

SUBSCRIBE for more free fashion sewing tutorials -   https://goo.gl/sJzW6P

Most recent upload -  https://goo.gl/Ee1bgz

Most Popular Tutorial on FSBTV -  https://goo.gl/m2YPuy



I just want to say thank you for watching, supporting and subscribing to my channel! If you enjoy the videos be sure to give it a THUMPS UP, HIT THE BELL, COMMENT and SHARE it with your friends.  If you havent already SUBSCRIBE hit the button so you dont miss out on the weekly videos and Ill will see YOU in the video.:)


Disclaimer: *Amazon affiliate links