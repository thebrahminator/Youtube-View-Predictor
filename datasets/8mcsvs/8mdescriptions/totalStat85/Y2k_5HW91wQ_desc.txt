Volkswagen Corrado VR6 - 1995 - Forza Horizon 2 - Test Drive Gameplay [HD]
------------------------------------------

Specs:
Xbox 360 Slim 
Roxio Game Capture HD Pro

------------------------------------------

Game Information:
Forza Horizon 2 is an upcoming racing video game developed for Microsoft's Xbox 360 and Xbox One consoles. It is the sequel to 2012's Forza Horizon and part of the Forza Motorsport series. The game will be developed by Sumo Digital for Xbox 360 and Playground Games for Xbox One, with Forza Motorsport series developer Turn 10 Studios supporting both builds.
__________________________________________