A variety of EMD locomotives, young and old, can be seen leading trains daily.  From SD40-2 pairs on trash, to SD70M-2 pairs on hot shot intermodals, and SD60's or SD70's on freights.

4-03-09
502 with an entire exConrail consist with a great Leslie RS-3L.

NS SD60M 6787
NS D8-40CW 8374
NS D8-40CW 8392

4-03-09
64J with a classic pair of the best locomotives ever, doing about Notch 6 hauling empties, with a great Leslie RS-3L.

NS SD40-2 3445
NS SD40-2 3424

4-03-09
261, almost missed him, but a great EMD duo with a exConrail SD60 trailing.

NS SD70M 2616
NS SD60 6707

4-04-09
21M, nice still-in-blue Conrail SD60M leader.

NS SD60M 6773 "CRQ"
NS D9-40CW 9877
NS ES40DC 7689

4-04-09
19G in some good light.

NS SD70 2566
NS D9-40CW 9678

4-04-09
69Q with an amazing SD60M duo, wild P3 horn!

NS SD60M 6780
NS SD60M 6767

4-04-09
65J struggles to pull trash loads up the hill at Robesonia and makes an amazing full throttle attempt with a pair of veterans!

NS SD40-2 3389
NS SD40-2 3383

4-05-09
16T with sweetness all over, EMD, CR, and OLS!

NS SD60M 6771
NS D8-40CW 8341 "CRQ"
NS D9-40CW 9251 "OLS"

4-06-09
20W, last but not least, a beastly combo.

NS SD70M-2 2759
NS SD60 6608

Stayed tuned for the next video, I guarentee it will be the most amazing train you'll ever see in 2009!