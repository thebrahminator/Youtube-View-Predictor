Cannonball Musical Instruments presents Don Menza playing a 12-bar blues on the Cannonball Stage at the 2011 Salt Lake International Jazz Festival.  He is playing a Vintage Reborn tenor saxophone in Dark Amber Lacquer.

with:
Russell Schmidt, piano
Jay Lawrence, drums
Jeff Campbell, bass

http://cannonballmusic.com