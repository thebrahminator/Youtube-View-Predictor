"WHEN THE GAME STANDS TALL" Trailer Deutsch German 2015 | Abonnieren  http://bit.ly/DVDKritik

Kanal: http://bit.ly/DVDKritik
Twitter: http://bit.ly/TWRobert
Facebook: http://bit.ly/FBRobert
Neue Vids: http://bit.ly/NeueVids1
Zweit-Kanal: http://bit.ly/RobsntownYT
Instagram: robsntown

# # # # # # # # # # # # # # # # #


Titel: WHEN THE GAME STANDS TALL
Originaltitel: WHEN THE GAME STANDS TALL
Deutscher Kinostart: 12. Februar 2015
Lauflnge: 115 Minuten
Altersfreigabe: FSK 6
Im Verleih von Sony Pictures

WHEN THE GAME STANDS TALL erzhlt die auf einer wahren Begebenheit beruhende Geschichte einer Highschool-Footballmannschaft whrend einer Saison, die fr das Team alles vernderte... Trainer Bob Ladouceur (Jim Caviezel; DIE PASSION CHRISTI) macht seinen Spartans zwar regelmig klar, dass es nicht allein ums Gewinnen geht. Dennoch haben er und sein Assistenz-Coach Terry Eidson (Michael Chiklis; The Shield  Gesetz der Gewalt) es fertig gebracht, die Mannschaft zu einer unglaublichen Rekordserie von 151 Siegen in Folge zu fhren. Die Bitte seiner Frau Bev (Laura Dern; JURASSIC PARK), sich wieder mehr auf die Familie zu konzentrieren, kommt fr Bob reichlich ungelegen. Denn gleichzeitig wchst der Druck auf ihn, die Siegesserie nicht reien zu lassen. Mitten in der von Krisen und tragischen Ereignissen geprgten Saison steht das Team pltzlich kurz davor, alles zu verlieren. Doch dann hilft ein bemerkenswerter junger Spieler (Alexander Ludwig; DIE TRIBUTE VON PANEM  THE HUNGER GAMES) dem Coach, sich daran zu erinnern, dass Teamwork in den alles entscheidenden Momenten schwerer wiegt als persnlicher Ruhm.

Trailer: Promotional use only.



Quelle Musik : 
Sneaky Snitch: http://incompetech.com
Author: Kevin MacLeod
Song: Cattailas
Lizenz: http://creativecommons.org/licenses/by/3.0/