We unbox the vivo x5max, a super slim mid-flagship smartphone at 5.08mm slim and comes with very respectable hardware onboard, stay tuned for our full review soon.

About KLGadgetGuy.com:
With the aim of exploring everything about tech, then educate and entertain people who wants to know more about it. KLGadgetGuy is a growing tech editorial/media based in Malaysia with a team of editors that tells you about the value of a tech product and service while we help tech partners further reach out their customers.
Visit us at www.klgadgetguy.com for tech news, gadget reviews and more.

We're on social media too!
twitter.com/klgadgetguy
fb.com/klgadgetguy
plus.google.com/+klgadgetguy
Instagram.com/klgadgetguy

Copyright KLGG Media Publications 2015.