Learn how to make paper in this fun experiment for kids. 
The App Store: http://bit.ly/itunesksne 
Amazon App Store: http://amzn.to/amazonksne 
Android Market: http://bit.ly/androidksne 
Nook App Store: http://bit.ly/nookksne 

Kid Science: Nature Experiments has dozens of activities for kids to try out at home. Learn about weather and the natural environment, and make paper, thunder, fountains and more! 

Learn more about this and other great apps from Selectsoft at www.selectsoft.com