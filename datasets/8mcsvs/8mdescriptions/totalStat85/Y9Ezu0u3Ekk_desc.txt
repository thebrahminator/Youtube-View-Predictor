Investing made simple: The Motley Fool's essential guide to investing is now available to the public, free of cost, at http://bit.ly/1atRpHZ. This resource was designed to cover everything that new investors need to know to get started today. For your free copy, just click the link above.

------------------------------------------------------------------------
In this edition of The Motley Fool's "Ask a Fool" series, Motley Fool One analyst Jason Moser takes a question from a Fool reader who asks: " What's your Foolish take on Starbucks?  What about their breathtaking P/E?  How about Realty Income? Jason talks about the number of growth opportunities that Starbucks still has today from the acquisitions of Evolution Fresh, La Boulange and Teavana to the addition of drive-thrus in their new stores. Further, Jason clarifies why Starbucks has such a high P/E ratio today and how investors should view the company. While Realty Income is a REIT that can generate higher dividend income, Jason also says why he's not sold on the long-term prospects of the company today.

Visit us on the web at http://www.fool.com, home to the world's greatest investing community!

------------------------------------------------------------------------
Subscribe to The Motley Fool's YouTube Channel: 
http://www.youtube.com/TheMotleyFool
Or, follow our Google+ page:
https://plus.google.com/+MotleyFool/posts

Inside The Motley Fool: Check out our Culture Blog!
http://culture.fool.com
Join our Facebook community:
https://www.facebook.com/themotleyfool
Follow The Motley Fool on Twitter:
https://twitter.com/themotleyfool