"Recuerdos de Ypacara"
Demetrio Ortiz & Zulema Mirkin

Performed by the St. Dominic Church Adult Choir
St. Dominic Music Ministry Orchestra
Vicente Chavarria, conductor

Ernesto Fernandez, flute; David Figueroa, violin; Anna Litvinenko, cello; Linda Deighan, double bass; Sahily Canovas, piano; Amelia Menendez, accordion

Frank Gomez, Maria de los Angeles Menendez & Marlen Ruiz, guitars; Oliver Mendez & Miguel Martinez, percussion

PROMUCOM presents the 2008 Summer Concert
St. Dominic Catholic Church, Miami, Florida
July 25, 2008