Europe perform their massive signature song "The Final Countdown" from their 1986 album of the same name, LIVE @ The O2 Empire, Shepherd's Bush, London, on Saturday, 19th February, 2011.

Europe is a Swedish hard rock band formed in 1979 and consists of vocalist Joey Tempest, guitarist John Norum, bassist John Levn, keyboardist Mic Michaeli and drummer Ian Haugland. Although widely associated with glam metal, the band's sound incorporates heavy metal and hard rock elements. Since its formation, Europe has released eight studio albums, three live albums, three compilations and seventeen videos.

Please feel free to leave a comment.

No copyright infringement is intended.