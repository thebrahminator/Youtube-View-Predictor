This is a cheap camera stabilizer useing nothing more than an Eyebolt and a few feet of string. 

A 1/4" coarse thread 20 TPI Eyebolt
A piece of string about twice your height

Simply tie the string in a loop running through the eye of the bolt.
Standing on the string will help you hold your camera steady.
Spreading the loop open will give you more stabilization.

When your finished useing it just put it in your pocket. No tripod to carry around.

My son Jeff was my cameraman http://www.youtube.com/user/Syn94