Website: http://neckandback.com
Forum: http://askspinedoc.com
Subscribe: http://www.youtube.com/subscription_center?add_user=neckandback
Appt: 970-479-5895

Connect with Dr. Corenman:
Facebook: http://www.facebook.com/neckandback
Twitter: http://twitter.com/#!/drcorenman
Back Pain Book: http://whybackshurt.com
Presentations: http://www.slideshare.net/neckandback
Images and Illustrations: http://www.flickr.com/photos/neckandback/
LinkedIn, visit: http://www.linkedin.com/in/drdonaldcorenmanspinesurgeon

How to prevent back injuries is an important topic for those who work or partake in activities where lifting is involved on a routine basis. Proper lifting techniques should be practiced. Injuries within the lower back are common and they usually lead to significant back pain. Improper lifting techniques during the lifting process are often what cause this back pain. 80% of the American population will suffer from lower back pain and while there are many things that can cause this, improper lifting can typically be found at the top of the list. The mechanism of lifting is actually very simple but yet can lead to injury in the lower back when done improperly. Often, it is slight actions and simple "bending over movements" that can lead to back pain. This video offers a simple diagram that describes the shock absorber and disc within the spine and what happens to these discs as individuals age and implement improper bending techniques. Twisting during lifting is the riskiest movement of all as it puts detrimental pressure on the spine. Twisting during bending will decrease the strength of the disc by 50%. Dr. Corenman provides an in-depth discussion about bending, twisting and improper bending techniques. He discusses how to prevent back injuries with TV8 Vail, and why these easy, proper lifting techniques are often times overlooked.

For more information, please visit Dr. Corenman's website http://neckandback.com and other web resources mentioned above.