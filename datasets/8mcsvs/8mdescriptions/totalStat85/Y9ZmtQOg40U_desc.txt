Manny and Frida choose to work off their many detentions by doing community service. They thought that it would be boring, but they've actually been assigned to help at Casa de Adios; an old folks' home for retired super villains!

Help save El Tigre by signing the petition:

http://www.gopetition.com/petitions/save-el-tigre.html  Viva El Tigre!

Disclaimer: El Tigre is copyright of Sandra Equihua & Jorge R. Gutierrez. I do not claim ownership for this awesome show.