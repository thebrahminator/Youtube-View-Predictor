Jesse Crosse takes to the rally stages in his Gp 4 Mk 2 Escort. This is the first round at the Hutton Kitchens Brands Hatch Summer Stages and the first chance to drive the new car in anger. Here are the edited highlights of Special Stage 3. 

Check out my Rally blog at http://www.carmagazine.co.uk/Communit...

Follow me on Twitter @jessecrosse

Want to stay up to date with CAR's videos? Subscribe here: 
https://www.youtube.com/channel/UC2xJqlJyVh0ASDwmOvMMYZA?sub_confirmation=1

Get the latest reviews and news from CAR magazine here:
http://www.carmagazine.co.uk/

CAR is the online edition of CAR magazine, Britain's oldest monthly motoring mag. It's the UK's best and fastest online motoring news service,  providing a constant stream of scoops, first drives and official pictures of new cars.