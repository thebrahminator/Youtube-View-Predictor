This Spring of 2010, our koi have grown larger.  Some have gained 7-8" in length.  There are new addition to our collection this year as you can see.  There's a 8" tancho showa that we kind of look forward to in the next 2-3 years.  Plus a few other small ones to come later this summer.


Show quality Japanese koi fish: tancho kohaku, ginrin showa, sanke, kin ki utsuri, shiro utsuri, Koromo, heisei nishiki, goshiki, platinum ogon. Mostly 1-4 year old fish ranging from 6"-24". This is my collection since Spring of 2008 from breeders such as Hoshikin, Dainichi, Ogata, Isa, Sakazume, Hoshokai.