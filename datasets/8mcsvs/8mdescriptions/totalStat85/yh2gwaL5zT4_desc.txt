Creer zelf een up-to-date look. Dat kan heel gemakkelijk en het is sowieso origineel. Het enige wat je nodig hebt, is deze DIY-video, tijd, Veritas-goodies en de creatieve inspiratie van Belmodo.tv. Have fun! 

Vorige week creerde ik een fringed tanktop. Ik kijk al vooruit naar volgende winter en knutsel mijn eigen variatie van de frill die Fendi voor dit seizoen de runway opstuurde. Perfect om te combineren met een hemdje en een high-waisted skirt of een pantalon.

Zin om ook zelf creaties te maken, check dan hier de site van Veritas, bij Veritas Brugge is er ook een eigen atelier waar je workshops kan volgen.