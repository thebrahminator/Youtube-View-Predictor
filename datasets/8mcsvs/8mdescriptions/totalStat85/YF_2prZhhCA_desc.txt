What you hear is a high quality midi track created by Finale, the composition software I use to write all my pieces, it is not a live performance.

No copyright infringement intended, Tik-Tok by Kesha is not owned by me, this is an arrangement of the original piece written for a string quartet group. 

Hear more compositions of mine at 
http://soundcloud.com/christos-paloukas