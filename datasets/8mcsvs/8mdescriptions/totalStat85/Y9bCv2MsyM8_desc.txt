England recorded their first victory of the Hockey Champions Trophy in Argentina as they defeated Japan 3-1 in their 5th/8th place match.

Hollie Webb made the breakthrough on 11 minutes, converting a penalty corner before Sophie Bray doubled the advantage following an incredible run from Sarah Haycroft to give England a 2-0 lead at half time.

Akane Shibata pulled Japan back into contention on 42 minutes turning in at the back post but Sarah Haycroft secured the victory seven minutes later with a superb shot into the far corner with an early contender for goal of the day.

ENGLAND: Maddie HINCH (GK), Kirsty MACKAY (GK), Laura UNSWORTH, Sarah HAYCROFT, Hannah MACLEOD, Georgie TWIGG, Susannah TOWNSEND , Susie GILBERT, Sam QUEK (C), Joie LEIGH, Alex DANSON, Giselle ANSLEY, Sophie BRAY, Hollie WEBB, Ellie WATTON, Shona McCALLIN, Lily OWSLEY, Zoe SHIPPERLEY 

JAPAN: Ryoko OIE (GK), Shiho SAKAI, Keiko MANABE, Kana NOMURA, Miyuki NAKAGAWA (C), Akiko OTA, Shiho OTSUKA, Nagisa HAYASHI, Mie NAKASHIMA, Akiko KATO, Akane SHIBATA, Maki SAKAGUCHI, Izuki TANAKA, Naho ICHITANI, Yuri NAGAI, Hazuki NAGAI, Sakiyo ASANO (GK), Shoko KANEFUJI

"Welcome to the FIH YouTube Channel where you can watch all the amazing skills, fearless keepers and incredible goals. Subscribe here - http://bit.ly/12FcKAW 

With master class videos and behind the scenes access you can get to know the star players from around the globe as they compete in the Hockey World League, World Cup and Champions Trophy.

Visit FIH on:
Facebook - http://www.facebook.com/fihockey  
Twitter - http://www.twitter.com/fih_hockey"