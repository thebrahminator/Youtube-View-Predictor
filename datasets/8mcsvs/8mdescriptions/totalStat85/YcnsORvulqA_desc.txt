Hello this is my Refresher Tide guide and I have been having lots of fun with it. Skip to 0:50 for ownage footage.

My Stats with Tide - http://i.imgur.com/Oz75V.jpg

**DISCLAIMER:**

This is NOT a pure support build. For this to work properly you HAVE TO let your teammates know that you are taking farm and somebody else should pick a hard support and buy wards, smokes, dusts etc. 

**Target Times:**
Persevearance Ring @ around 20m
Oblivion Staff @ around 25m
Refresher Orb @ around 30-35m

**Items:**

*Early game core*

Arcane boots, Bracers, Bracers, Ring of Regen, 2 TP scrolls

*Mid game core*

Arcane boots, 2x bracers, Refresher Orb, 2 TP scrolls 

*Late game core*

Arcane boots, Drums of Endurance, Refresher orb, 2 tp scrolls, Null Talisman. (if you have the only arcane on your team)

**Gameplay:**

Lane with a ranged hero. Take anchor at lvl 1 and max it as fast as you can. Try to harass the enemy and take last hits at the same time with your Anchor Smash. We do not priotirize Gush because its not spammable.

As soon as you are lvl 6, try to get a kill or assist asap that will help u build the Ring of Regen. Once u get it you can leave lane and go jungle. Always have tp with you and as soon your ult is up, use it and try to get an assist or get a kill yourself. Also TP if your towers need defending. Make sure to let your team know about your intentions and make sure they do not team fight without you.
The only other item you can get is a Null talisman in case you are the only person on your team with arcane boots. But you don't have to have it.

Don't forget you are not that tanky, when your refresher+ult is up try to stay back a little because the enemy team will focus you eventually.