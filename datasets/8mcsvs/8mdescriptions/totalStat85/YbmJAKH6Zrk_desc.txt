http://www.skedco.com

Complete SKED Military Rescue System

http://www.skedco.com/default.aspx?categoryID=2&subID=51

High Performance Rescue Hoist Accessories

http://www.skedco.com/default.aspx?categoryID=2&subID=41

SKEDCO HELITAG HELICOPTER TAG LINE KIT
NSN: 6545-01-381-0654
SK-1010

http://www.skedco.com/detail.aspx?categoryID=2&subID=41&sku=SK-1010

The Helitag Kit is designed to prevent litter spin during hoist operations. A litter can spin up to 180 rpm's. That is enough to cause a patient to become a "human centrifuge". The body fluids are forced to the ends of the body. Litter spin can cause death if allowed to continue. The helitag kit when properly used will prevent litter spin completely. 

This kit contains 250 feet of high visibility 7mm water rescue rope. The break strength of the rope is 1,900 pounds. Because the core is polypropylene and the sheath is nylon, the rope floats. It should not be used for rappelling. At the litter end of the rope, there is a "weak link" that breaks between 265 to 310 pounds. It is a safety "break away" that separates if the rope becomes entangled in trees or boat and ship masts. It is attached to the V strap with 2 stainless steel screw links. The V strap is attached to the litter with 2 Petzl key-lock carabiners (snap links). The kit is all contained in a 7 inch diameter x 23 inch camouflage bag. This is required onboard all Army MedEvac helicopters and in many infantry medical sets.

SKED-EVAC CASUALTY MARKER AND SIGNAL KIT
SK-12200-XX

http://www.skedco.com/detail.aspx?categoryID=2&subID=26&sku=SK-12200

SKED-EVAC CCP/HLZ MARKER
SK-12201

http://www.skedco.com/detail.aspx?categoryID=2&subID=26&sku=SK-12201

VS-17 Panel Marker
OP-282001

http://www.skedco.com/detail.aspx?categoryID=2&subID=26&sku=OP-282001

Used by the U.S. Army Signal Corps and Pathfinders, these laminated nylon panel markers are a sure attention getter. Measures 70 in. x 24 in. Fluorescent orange on one side and hot pink on the other. Each end has 2 tie straps and 3 snaps, and the middle has just the 2 tie straps. Comes in 2 per pack.

Rescue Flash Signal Mirror
sk-12202

http://www.skedco.com/detail.aspx?categoryID=2&subID=26&sku=SK-12202

Visible over 20 miles, durable LEXAN polycarbonate mirror with mil-spec retro-reflective aiming aid for one-handed use.

SKED-EVAC CASEVAC Conversion Kit, Aircraft
NSN: 6545-01-536-9315
SK-12100
http://www.skedco.com/detail.aspx?categoryID=2&subID=100&sku=SK-12100
  
Helicopter Crew Chief Bag - Black
NSN: 6545-01-538-9464
SK-1151
http://www.skedco.com/detail.aspx?categoryID=2&subID=100&sku=SK-1151
  
Helicopter Medic Bag - Black
NSN: 6545-01-538-8494
SK-1150 
http://www.skedco.com/detail.aspx?categoryID=2&subID=100&sku=SK-1150

NOTE: when you watch a SKEDCO video, YOUTUBE will offer "Related Videos" in a box to the right that ARE NOT SKEDCO VIDEOS; don't assume they are our videos when they are not; only videos on this channel are SKEDCO videos. If you click on other videos you can interact with their owners since we don't have any control over them