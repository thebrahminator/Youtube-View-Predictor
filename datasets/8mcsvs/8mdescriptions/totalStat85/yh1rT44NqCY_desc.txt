Millie's got a Minecraft tutorial that's totally chill - an actual working Fridge full of cake and sweets! Like this video if you wish you had one in real life, too!
Minecraft Death Run! - http://bit.ly/1IWsUdG
From all across the Internet, the greatest kid game commentators have gathered for one purpose: to play the coolest games EVER! Each episode an awesome YouTuber host tries out a new app, mod or level. Enter into a world of epic Minecraft builds, addicting Candy Crush speed runs, hard hitting Super Smash Bros brawls and every game in between!
 Credits 
Hosted by Millie Ramsey from https://www.youtube.com/gamekids
Producer: Chelsea Harfoush
Director: Geoff Ramsey
Editor: Trevor Collins

Follow DreamWorksTV! 
instagram - https://instagram.com/dreamworkstv/
twitter - https://twitter.com/dreamworkstv
facebook - https://www.facebook.com/dreamworkstv


DreamWorksTV is the ultimate YouTube destination for kids and families. See what Shrek and Donkey, Po and the rest of your favorite DreamWorks characters are up to! Hack your life with helpful DIY tips and tricks, sing along to todays catchiest songs and laugh out loud to original animated series. DreamWorksTV has it all! Check back daily for new episodes. 

 Watch Something New!  
http://bit.ly/1L3zRrF


 SUBSCRIBE TO DreamWorksTV!  
http://bit.ly/1kulRcU