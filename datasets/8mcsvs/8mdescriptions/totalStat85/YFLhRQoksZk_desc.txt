Made by Harry Steele 

This video gives you information about:
0:25 Amazon's Location
0:52 Sections of the rainforest (Emergent layer, Canopy layer, Lowerstory layer, and the forest floor layer)
3:12 Amazon Rainforest's Importance to the worlds
3:52 Human impact
4:24 What can be done to help the Amazon rainforest survive