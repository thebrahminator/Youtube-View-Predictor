Nacht und Trume D827 (Franz Schubert) arr. Cello & Piano
Sarah Acres (Cello) & Albert Combrink (Piano)

Published in 1825 as Op.43 No 2, D.827 was composed in the winter of 1822/1823. It is remarkable in Schubert's "Night Song" ouevre, and it remains one of the pinnacles of his output. Notoriously difficult to perform and deceptive in its simplicity, it makes a perfect transition to the cello.

Recorded live at Erin Hall, Cape Town in "Reflections", a Cello Meditation programme. 

Read more about this song (Including the Original German Text and English translation and FREE SHEET MUSIC Downloadable in Pdf Format) HERE:
http://www.albertcombrink.com/2013/11/09/erinhallnachtundtraumecello/

Website: Sarah Acres - http://www.facebook.com/CellistInTheCity
Website: Albert Combrink - http://www.albertcombrink.com
Twitter: @albertcombrink  - https://twitter.com/albertcombrink

For similar music recorded at a meditation programme:
http://www.youtube.com/watch?v=4Y-oDfA-Y_0&list=PLxCpEk4IkEyHIGwewQlDOeLL91bdZtJFO