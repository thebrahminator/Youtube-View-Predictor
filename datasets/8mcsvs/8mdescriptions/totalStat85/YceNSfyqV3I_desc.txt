Ready to switch to a raw food diet? You can get instant access to a FREE e-course: "How to Go Low-Fat Raw in 21 Days" by signing up now at: www.rawsomehealthy.com

In this part 2 of a (2 part) video response to a raw food promoter Dr Gabriel Cousens we address the following issues: 

- Why you should never be afraid of eating more than 2 bananas a day.
- Why sugar does not cause type 2 diabetes and promote cancer.
- Why high fat diets are the least optimum for health and longevity.
- Why fructose from whole fruits does not cause inflammation of the brain.
- Why plant fats are not ok in large quantities for health.
- Why high fat raw vegan foodists often bump into health problems.

Grab your copy of Deliciously Raw Dinners to enjoy low-fat raw vegan savoury recipes:   
http://rawsomehealthy.com/deliciously-raw-dinners-3/

Transform you life and your health and download The Spirit of Transition book:
http://rawsomehealthy.com/the-spirit-of-transition/

Donate to Rawsomehealthy Channel 
http://rawsomehealthy.com/donation/

Thank you.