The Getac V100 -Ex Rugged Laptop transforms with one quick rotation to a Rugged Tablet PC. Designed with the real-world in mind, the Getac V100 -Ex is equipped with an innovative touch screen. The Getac V100 -Ex is both MIL-STD 810G certified and IP65 compliant, composing a magnesium alloy case, a shockprotected HDD, vibration and drop resistance, and sealed I/O caps and doors to prevent damage from solid particles and moisture.

Homepage: http://www.ecom-ex.com/en/products/mobile-computing/laptops/product-information/v100-ex/detail/

With an Ultra Low Voltage Intel Core2 Duo Processor, the Getac V100 -Ex boasts a quiet fanless design and the power you need for various sophisticated applications.


The Getac V100 -Ex features a 10.4" LCD display. A full-sized 83-key keyboard is included. The Getac V100 -Ex is great for field applications: it includes an integrated 2.0M pixels webcam, and can also be customized to include a built-in GPS receiver and antenna.


The Getac V100 -Ex provides comprehensive connectivity options including Bluetooth, WLAN, WWAN and the advanced TPM security to safeguard important data.


The Getac V100 -Ex is the first product of a joint venture between ecom instruments GmbH, Germany (www.ecom-ex.com) and GETAC Technology Cooperation, Taiwan (www.getac.com). With Getacs knowledgement in building rugged mobile devices and ecoms reputation in intrinsic safety such state-of-the-art technology is now available even inside hazardous areas.