Om olyckan skulle vara framme !
Med en bilkamera fr du lttare rtt i din sak gllande frskringsrenden 
En bilkamera kallas ibland ven fr dashcam 

Bilkamera till grossistpriser ! 013-9913935   cam@nordtrack.se

En bilkamera vl vrd namnet! Lttast r att lta kamerans imponerande specifikation tala fr sig sjlv...
Full-HD upplsning & bildfrekvens p 1920x1080 vid 30fps, respektive 1280x720 vid 60fps ger en knivskarp bild av vad som hnder ute p vgarna. Vidvinkel p hela 140 grader tcker upp det mesta utanfr vgrenen, och en stor, skarp och bekvm 2,7 tums LCD-display ser till att du ltt och snabbt p plats kan spela upp dina nytagna actionrullar.

Nordic Savage 5 finns bde som modell med och utan GPS-mottagare. Modellen med GPS-mottagare visar dina exakta koordinater och hastighet i bildens ena hrn. GPS-funktionen gr s klart att stngas av nr s nskas.
Om olyckan skulle vara framme knner kamerans inbyggda G-sensor av kollisionen och sparar automatiskt inspelningen tiden kring hndelsen s att denna inte kan spelas ver av kamerans loop-funktion.

Om du vljer att aktivera bilkamerans rrelsedetektion s startar kamerans inspelning automatiskt vid rrelse framfr bilen. Ett mycket effektivt stt att dokumentera eventuella skador som sker p din bil nr du inte ser p.
Som alltid kopplas bilkameran enkelt in med cigg kontakt i bilen, och kameran startar automatiskt sin inspelning nr bilens spnning sls p, respektive stnger av sig sjlv nr spnningen sls av.
Bilkameran levereras med USB-kabel fr smidig filverfring till datorn. Montering sker med den medfljande sugproppen som du enkelt monterar p insidan av bilens vindruta. Passar alla bilar.