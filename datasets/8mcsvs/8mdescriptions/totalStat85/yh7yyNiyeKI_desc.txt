http://www.clarkycat.co.uk
http://www.myspace.com/clarkycat
http://www.myspace.com/ismly

As tightly wound as a coiled spring 'Clarky Cat' have arrived to take you by the scuff of the neck.

Their debut output is the electrifying single 'Sightline' that pronounces all that is exciting about what it is to be young and in the here and now. Throughout 'Sightline' members Anne, Mark, Cardiac and Edith add synths and samples, frenetic bass and melting guitars to Clarky Cat's twisted pop sensibilities to create a high octane racket that'll hit your ears in aural technicolour.