Another part of parenting they don't tell you about. 7 minutes of an emotional roller coaster as I try to get a 5 year old to eat her soup.


Dont forget to like and subscribe to the Hoag Vlog 
https://www.youtube.com/playlist?list=PL_kXfBi2NNa3MusCzCgAduGSiY8VYmhcQ


Instagram - https://www.instagram.com/hoag
Twitter - https://www.twitter.com/_hoag_
Facebook   https://www.facebook.com/matthewdavidhoag
Snapchat - @lakid07

For Wedding and Event Video:

www.lakidmedia.com
Instagram  https://www.instagram.com/lakidmedia
Facebook  https://www.facebook.com/lakidmedia
Email  lakidmedia@gmail.com