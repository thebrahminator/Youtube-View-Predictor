Flight operations aboard the aircraft carrier USS George H.W. Bush (CVN-77), as F/A-18 Hornets and EA-6B Prowlers are recovered and launched during surveillance, and reconnaissance missions. The GHW Bush is the tenth and final Nimitz-class supercarrier of the United States Navy, and is currently stationed in the Persian Gulf. Filmed September 16, 2014.

Film Credits: Damon Moritz