Will the Playstation 4 succeed--or fail--purely on the basis of its specs and features? Turns out there's not a neat equation to predict console success, as we retroactively examine the last few generations.
Subscribe for new episodes every Wednesday! http://bit.ly/SubToEC (---More below)

(Original air date: March 20, 2013)
_______

Get your Extra Credits gear at the store! http://bit.ly/ExtraStore
Play games with us on Extra Play! http://bit.ly/WatchEXP

Watch more episodes from this season of Extra Credits! http://bit.ly/2qTCHK5

Contribute community subtitles to Extra Credits: http://www.youtube.com/timedtext_cs_panel?c=UCCODtTcd5M1JavPCOr_Uydg&tab=2

Talk to us on Twitter (@ExtraCreditz): http://bit.ly/ECTweet
Follow us on Facebook: http://bit.ly/ECFBPage
Get our list of recommended games on Steam: http://bit.ly/ECCurator
_________

Would you like James to speak at your school or organization? For info, contact us at: contact@extra-credits.net
_________

 Intro Music: "Penguin Cap" by CarboHydroM
http://bit.ly/1eIHTDS

 Outro Music: "Night City Funk" by Anti-Syne
http://ocremix.org/remix/OCR02248/