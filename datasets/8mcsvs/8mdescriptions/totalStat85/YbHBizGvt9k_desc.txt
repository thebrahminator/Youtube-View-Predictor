Music By http://www.youtube.com/Evanlessons

The always-been-there rivalry among the influential German car makers saw another interesting bout of fight when Audi launched the Audi Q7 SUV in the year 2006 in order to counter the popularity of Porsche Cayenne, another popular car in the same segment. Audi's strategy did work out well. In no time, Audi Q7 became one of the most sought after cars in the SUV segment. Q7 was registering a huge number of sales and great profits.

But as the time passed by, things changed. The rules and regulations that governed the car segment changed. But Audi is no faint-heart company. These changes led Audi to change its strategy according to them. As a result, Audi put a plan on paper to make the Q7 in sync with the new guidelines. The next generation 2013 Audi Q7 is going to be much lighter and smaller and hence will be lighter on your pockets as well.
Since its inception in 2006, Audi Q7 has been based on the platform of Volkswagen Touareg and Porsche Cayenne. The 2013 model of Audi Q7 will be no exception as it is going to be based on the same platform. In its attempt to make the car much lighter and smaller than the models of previous years, it is decided to use aluminum as much as possible in the construction of the car which has let it shed approximately 650 pounds of weight. Use of hybrid technology with the Q7 is also been in the news, so expect a hybrid Q7 sooner.
The Audi Q7 will be powered by a 3.0 liter supercharged V6 engine which can produce 280 hp of maximum power and 295 lb-ft of torque. It comes with an eight speed auto transmission. It is needless to say that it is an all wheel drive vehicle. The car can deliver quite a respectable fuel economy as it gives 16 mpg in city and 22 mpg on highway. The combined efficiency stands at 18 mpg. It is able to clock 0 to 60 in just 6.9 seconds.
On the other hand the Audi Q7 TDI comes with 3.0 liter turbo diesel V6 that can generate 225 hp of power and 406 lb ft of torque. This is able to clock 0 to 60 in just 8.6 seconds. The fuel economy stands at 17 mpg, 25 mpg and 20 mpg in city, highway and combined respectively. The torque in the diesel version is amazing to pull the car in any road conditions.
The interiors of the 2013 Q7 comes with an amazing feel and comfort. You would feel the luxury as soon as you enter in the car. The seats are made up of the top notch material in the segment. Also the tilt and telescoping steering wheel among other options make the car look more luxurious. The dashboard is fine tuned and the things don't look cluttered on it.
The exterior of the car also has its own charisma. The Audi feel can be easily felt when one looks at it. At the same time, the car comes in a large body mass. But it has become smaller than the previous versions anyway.
The 2013 Audi Q7 should be priced around $45000. Rest depends on the options and packages one decides.

Read more: http://www.audipage.com/2012/03/2013-audi-q7-review/#ixzz1tzDzTDTX

Q7 2012
Audi has added their new Audi Connect to the MMI system, which adds internet-driven POI search, via user input or via the voice control system, as well as access to online services delivering local fuel prices, news, weather and other information. Audi Connect also offers in-car WiFi connectivity for up to 8 devices.
[edit]US models
The vehicles were introduced in 2009 as 2010 models. Available models include 3.6 FSI quattro, 4.2 FSI quattro, 3.0 TDI clean diesel quattro.
Changes include standard LED taillights with available LED turn signals and daytime running lights, SIRIUS traffic system.
Production version went on sale in September 2009.[22]
For 2011 the 3.6 FSI and 4.2 FSI were replaced by two 3.0 L TFSI supercharged engines. The base version develops 272 hp (203 kW) (280 hp (210 kW) for 2012) and 295 lbft (400 Nm) while the S-Line has 333 hp (248 kW) and 325 lbft (441 Nm).