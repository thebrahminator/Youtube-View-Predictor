SmartCast Wireless HD Mirroring Video Receiver
Features:
Real-time wireless mirroring on TV
The most sophisticated home entertainment devices
Full HD 1080P display
Full mirror for IOS devices
Built-in WiFi module
Media play directly from USB
Compatible with Miracast, DLNA
Connect wireless internet access freely, no need to install any software for the mobile phones and tablet pcs running android 4.2 system 

General Specs
Model V5i
Operating System Linux
CPU MIPS 500MHz
RAM 256MB DDR3
Flash 128MB Nand
Wi-Fi IEEE 802.11b/g/n
HDMI 1.3
Display Full HD 1080P
Power 5V / 500-1000mA
Compatibility Miracast, DLNA
Package Details
Weight: 187.25 g 
Size: 3.8*8*5.2 cm