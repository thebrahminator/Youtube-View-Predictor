We visited the Vancouver based set for a look at what we can expect, and also for exclusive on-set interviews with the cast. We were able to speak with new series regulars Robert Picardo (Richard Woolsey) and Jewel Staite (Dr. Jennifer Keller), as well as returning cast members Rachel Luttrell (Teyla Emmagan), David Hewlett (Dr. Rodney McKay), and Jason Momoa (Ronon Dex).

For more TV videos, TV episodes, TV reviews, celebrity interviews and more, go to:
http://www.tvweb.com

Connect with other TV fans on Facebook:
http://www.facebook.com/pages/TVweb/167115801169

Follow us on Twitter:
http://twitter.com/tvweb