Watch the IOLO teaser here: http://youtu.be/XQk_BcVtufg

"Although I don't fear dying, I still would rather not." 
- Yurnero, the Juggernaut

Short movie about originally lost game on a public EU server with a sudden (in Dota terms, i.e. 30 minutes long) twist.
Co-starring Silvester Stallone as Rocky Balboa.

Match ID: 92018248 (expired)

Why two first bloods: Editing mistake :(added wrong Naga sound)

Playlist:
0:05 - http://youtu.be/gad6xWW81Uk
0:29 - http://youtu.be/-r7woqe0A1k
0:37 - http://youtu.be/VJCn3LxRo3w
0:54 - http://youtu.be/NZcQGImWq0E
1:46 - http://youtu.be/MlPFxPESay4
2:22 - http://youtu.be/v6ziKuWe5BE
3:31 - http://youtu.be/ldziKLeSroU
3:54 - http://youtu.be/wyz_2DEah4o
4:10 - http://youtu.be/hkM98waziao
4:38 - http://youtu.be/l3ajaLVrLSk

Bonus stuff:
Wallpaper 1600x900: http://i.nahraj.to/f/ihT.jpg
Animated gif: http://i.nahraj.to/f/ihV.gif
Sony Vegas project screenshot: http://i.nahraj.to/f/ihW.jpg

__________

Facebook: http://www.facebook.com/hitpoint.cz 
Web: http://www.hitpoint.cz
Twitter: http://twitter.com/hitpointcz
Twitch: http://www.twitch.tv/hitpointcz