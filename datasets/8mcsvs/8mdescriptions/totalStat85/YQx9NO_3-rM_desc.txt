10 de Octubre de 1993
Fecha 22 del Descentralizado-Copa Faucett 1993

Alianza era puntero con 32 puntos y la U venia 2do con 30. Partido decisivo en Matute.

Alineaciones:
U: Zubzuck; Charun, Reynoso, Asteggiano, Bravo; Rodriguez, Carranza, Martinez, Nunes; Balan Gonzalez, Baroni.
Alianza: Jacinto Espinoza; Jayo, Wilmar Valencia, Frank Ruiz, Guido; Jose Soto, Kanko Rodriguez, Marco Valencia; Juan Saavedra; Waldir Saenz, Muchotrigo.