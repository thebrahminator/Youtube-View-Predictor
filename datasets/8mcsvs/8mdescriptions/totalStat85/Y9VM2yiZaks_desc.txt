More @ http://www.facebook.com/EFPlayaTamarindo
More videos of Tamarindo @ http://www.tamarindobeach.ws
EF Playa Tamarindo
With a tropical climate and plenty of sun, life in Playa Tamarindo revolves around enjoying the surrounding golden beaches, national parks and mountains. 
Contact us: ef.tamarindo@ef.com
Telephone: (506) 2653-2095.
Visit an active volcano, watch giant turtles hatch, go trekking in a rainforest or learn how to surf. Build your Spanish skills at our campus school, just minutes from some of the best beaches in Costa Rica.
In EF playa Tamarindo you can combine your course with volunteer work, surf classes or salsa classes.  Start day every Monday! Book Now!