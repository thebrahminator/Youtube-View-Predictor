Batman: TAS/Gotham Knights - Over the Edge AMV featuring the smash hit from the Batman Begins Soundtrack, Molossus!  Zimmer and Howard rock!  This is perfect chase music!  Enjoy.

Music: "Molossus" from the Batman Begins soundtrack by Hans Zimmer and James Newton Howard


I do not own Batman or music.  No copyright infringement intended.