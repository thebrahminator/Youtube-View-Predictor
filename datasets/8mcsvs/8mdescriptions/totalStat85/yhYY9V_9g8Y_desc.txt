ASK 3 Star Furniture shop is engaged in providing a wide range of wood cots and steel Cots. The range of wood cots offered by us is designed using super premium quality material to enhance strength and durability. Steel cots are available in different shapes and sizes as mentioned below, our range can also be customized as per the specific requirements of the valued clients.

Single Cot (Size 75" X 30")
Medium Cot (Size 75" X 36")
Double Cot (Size 75" X 42")
Super Cot (Size 75" X 48")
Custom Size Cot
Hostel Cots
Flywood Cot (Size 75" X 48")
Flywood Cot (Size 75" X 60")
Flywood Cot (Size 75" X 72")