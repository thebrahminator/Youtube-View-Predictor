Were big. Bigger than you probably expect. In fact, were the 4th largest car manufacturer in the world.

As you might imagine, that makes for a large operation  which is why you can find the Hyundai name on everything from microchips to super tankers, why we operate the worlds largest integrated automobile manufacturing plant and why were the only car company in the world to have our own steel mills. 

We actually have three of them  enabling us to make the best possible steel with raw materials sourced from Australias backyard. We do this because we know the best steel makes for the strongest cars.

http://www.hyundai.com.au/why-hyundai