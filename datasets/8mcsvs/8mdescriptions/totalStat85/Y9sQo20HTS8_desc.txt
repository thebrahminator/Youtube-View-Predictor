Welcome to Clubclass English Language School Malta.  
Clubclass is the only residential English language school in Malta where the school is situated WITHIN the same building as the Accommodation, Indoor and Outdoor Pools, Fitness Centre, Aerobics Studio, Games Room, Internet Cafe, Launderette, Pizzeria, Pub, Restaurant & more.
Clubclass is licensed by the Malta Tourism Authority and the Ministry of Education, fully accredited by FELTOM and a full member of WYSETC and ALTO.  
Courses are offered in General English, Business / Professional English, English for Specific Purposes; students are also prepared for TOEFL, IELTS and University of Cambridge EFL examinations.
Club Class English Language School Malta, your choice for English courses in Malta.