FINAL ADVERTISED PRICE: $17,500 - - - - STOCK #041219

This boat has been sold  (Heading to a buyer in Pittsburgh, PA).  Viewers who viewed this boat were most likely to view the following comparable listings, which are still for sale:

1985 Chris-Craft 314S Stinger in Hollywood, Florida - $22,500
https://www.popyachts.com/boats-for-sale/chris-craft-314s-stinger-68844

2009 Triton 18 Explorer in St Louis, Missouri - $18,500
https://www.popyachts.com/boats-for-sale/triton-18-explorer-83788

1986 Nordic Boats Viking in North Ft Myers, Florida - $11,900
https://www.popyachts.com/boats-for-sale/nordic-boats-viking-95411

1996 VIP 2400 Vindicator in Coden, Alabama - $12,000
https://www.popyachts.com/boats-for-sale/vip-2400-vindicator-91970

1988 Scarab III in Port Saint Lucie, Florida - $19,900
https://www.popyachts.com/boats-for-sale/wellcraft-scarab-iii-21665

1999 Skeeter 200 SL in Covington, Louisiana - $15,000
https://www.popyachts.com/boats-for-sale/skeeter-200-sl-89644

2006 Skeeter SL 190 S&F in Springfield, Missouri - $13,000
https://www.popyachts.com/boats-for-sale/skeeter-sl-190-sf-62635

2012 Nitro Z7 in Beach City, Texas - $25,000
https://www.popyachts.com/boats-for-sale/nitro-z7-104081

1999 Ranger Boats R97 in Sugar Hill, Georgia - $14,000
https://www.popyachts.com/boats-for-sale/ranger-boats-r97-103509

2004 Ranger Boats Reata 180VS in Bryan, Texas - $15,500
https://www.popyachts.com/boats-for-sale/ranger-boats-reata-180vs-99981

List your Boat with POP Yachts at no cost.  We believe in being Paid on Performance, therefore WE ONLY GET PAID IF WE SELL YOUR BOAT!

Click below for a quick introduction to the selling process:
https://www.popyachts.com/sell

Or call (941) 357-4044 to get the process started right now.