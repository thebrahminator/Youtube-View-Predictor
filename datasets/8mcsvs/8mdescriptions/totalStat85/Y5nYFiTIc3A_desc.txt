The "Admission Insider" Karen Kesteloot looks at a student's first draft portfolio for admissions to an Illustration program. In the video, Karen discusses what you, as an aspiring illustrator, need to include in your art portfolio - he talks about the importance of life drawing samples and "WOW" factors that you need to consider in your art portfolio.

---

If you need to build up your life drawing samples to be include in your art portfolio but can't find models to help you out, please read "No Nude Models? No Worries! 6 Alternative Life-Drawing Resources for Art Students." Below is the link to the article.
http://portprep.com/wp/2013/04/life-drawing-resources-students/

About PortPrep.com
PortPrep is a website owned by Karen Kesteloot and is dedicated to high school seniors who want to know how to make an art portfolio that will get them into the best art and design colleges! For more information, go to http://portprep.com