This is the complete collection of Hot Wheels track accessory packs sold exclusively at Toys R Us stores. Here I'll demonstrate the Jump, Loop, Launcher, Gravity Clamp, Crash Section, Track Curve, and Booster. Most of the packs sell for around $5 and the Booster is around $15. These pieces are excellent for building an exciting Hot Wheels stunt track setup. 

Check out my other videos!

Hot Wheels Track Pack Stunt Set with Loop Jump Launcher
https://www.youtube.com/watch?v=rZnR2YGekjY

Hot Wheels Workshop 2 Speed Power Booster Stunt Track Accessory
https://www.youtube.com/watch?v=oPkWPth1psc

Hot Wheels Super 10 in 1 Track Playset
https://www.youtube.com/watch?v=k4RT1kRDN3c

Hot Wheels Serpent Cyclone Track Set
https://www.youtube.com/watch?v=NzQbOUcgpu0

Hot Wheels Super Track Pack Playset
https://www.youtube.com/watch?v=0xOd_0bKGNI