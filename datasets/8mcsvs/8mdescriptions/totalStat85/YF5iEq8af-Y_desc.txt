( http://www.TFLtruck.com ) The 2015 Ford Expedition get an update for with minor exterior changes, but the big news is under the hood. The old V8 is gone, and in its place is the 3.5L EcoBoost V6 with 365 horsepower and 420 lb-ft of torque. How does this long EL 4x4 Expedition do at 0-60 MPH? What if we load it with four people? How would it affect the acceleration times?

( http://www.patreon.com/tflcar )  Please visit to support TFLcar & TFLtruck.

Check us out on:
Facebook: ( https://www.facebook.com/tflcar )
Twitter:     (  https://www.twitter.com/tflcar )
and now even Truck Videos on YouTube at:
The Fast Lane Truck ( http://www.youtube.com/user/tflcar )
and classic cars as well at:
TFLClassics ( http://www.youtube.com/user/ClassicsUnleashed )