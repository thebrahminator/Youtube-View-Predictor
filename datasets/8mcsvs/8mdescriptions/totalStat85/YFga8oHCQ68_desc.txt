Celebrity makeup artist Amy Adams shows us the looks that she created for a beauty photoshoot using rimmel products. She begins by showing us how to create the Wake Me up look by firstly using Wake Me Up foundation which makes you look like you have slept for 8 hours even if you haven't.

Celebrity makeup artist Amy Adams shows us the looks that she created for the Fabulous beauty photoshoot using rimmel products. She begins by showing us how to create the Wake Me up look by firstly using Wake Me Up foundation which makes you look like you have slept for 8 hours even if you haven't.

She begins by applying the foundation all over the face using a sponge. Make sure you apply the foundation right along the jawline and all the way down the neck. In this look Amy is cheating the healthy and glowing just woken up skin. The perfect product to achieve this is the Wake Me Up Instant Radiance Shimmer Touch in Radiant Rose which Amy dabs lightly on to the cheeks.

Now that Amy has achieved a gorgeous rosy skin all that is left is to dress up the eyes and lip. To do this Amy shares her secret of using Stay Glossy Lipgloss in Endless Night and dabs a little bit on to the eyelid and lips. This is how you create a gorgeous glowing just woken look.

Now that Amy has finished showing us her daytime fresh look, she now shows us an evening smokey eye look with a very dark vampy mouth which is on trend this season. She first uses Wake Me Up Concealer to prime the eyelids to help the powder eyeshadow stay on for longer.

To achieve the smokey eye look Amy uses Glam'Eyes Quad Eyeshadow in Sun Safari and Smokey Brown. She uses the darkest brown on the eyelid all over top of the concealer which will help the eye makeup stay on all night. Amy then uses the applicator brush to line the lower eye lashes using the same shade to help prevent any spills of eyeshadow under the eyes. Next Amy applies Scandaleyes Volume Flash mascara which gives you a great big false lash effect.

To complete the look Amy creates a dark dramatic lip which is really popular on the catwalk this season. Amy firstly lines the lip using Lasting Finish 1000 Kisses Lip Liner in Black Tulip which keeps your lipstick from bleeding. Next using Lasting Finish Lipstick in Starry Eyed Amy applies this over the top of the lip liner using a brush to give you great staying power.

Amy then shows us how to create the greasy cool rock chick look. She starts by toning down the lips using the Wake Me Up Concealer using the darkest shade and then dabs the concealer over the lips to take the rosy pink colour out to make them more nude to create a gorgeous glowing finish.

Rimmel Products Used

Wake Me Up foundation in Ivory
Wake Me Up Instant Radiance Shimmer Touch in Radiant Rose
Stay Glossy Lipgloss in Endless Night
Wake Me Up Concealer
Glam'Eyes Quad Eyeshadow in Sun Safari and Smokey Brown
Scandaleyes Volume Flash Mascara in Black
Lasting Finish 1000 Kisses Lip Liner in Black Tulip
Lasting Finish Lipstick in Starry Eyed .

Stay up to date with the latest from Rimmel HQ:
Instagram: https://www.instagram.com/rimmellondonuk
Facebook: https://www.facebook.com/rimmellondon
Twitter: https://twitter.com/rimmellondonuk
Website: https://uk.rimmellondon.com

Welcome to the official Rimmel London Youtube channel: your go to guide to perfecting the latest trends. Obsessed with the London look, well show you how to #GetTheLook with make-up and nail tutorials, as well as giving you a backstage pass to all the hottest events. Subscribe to our channel today to make sure youre the first to know whats going on at Rimmel London HQ.