The 2012 US Youth Soccer National Championship Series is the country's most prestigious national youth
soccer tournament. The event provides over 185,000 players from US Youth Soccer's 55 state associations,
the opportunity to showcase their soccer skills against the best competition in the nation while emphasizing
teamwork, discipline and fair play. Learn more http://championships.usyouthsoccer.org