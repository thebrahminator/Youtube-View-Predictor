Test drive of changes in Minecraft Weekly Snapshot 12w03a, including the new Zombie path-finding!

Get the Minecraft snapshot update here:
http://mojang.com/2012/01/19/minecraft-snapshot-12w03a/

BLOG: http://paulsoaresjr.com
TWITTER: http://twitter.com/paulsoaresjr
FACEBOOK: http://www.facebook.com/PaulsoaresjrPub
YOUTUBE: http://www.youtube.com/user/paulsoaresjr
FAQ: http://www.youtube.com/watch?v=bp9NTrLOdCM

Get Minecraft here:
http://minecraft.net