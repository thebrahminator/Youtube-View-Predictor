All project resources(video, parts list, guides, FAQ, etc):  http://goo.gl/SyRLb5
Accepting a limited number of send-in modifications and assembled circuit board orders. dekuNukem@gmail.com
Source code and schematics: https://github.com/dekuNukem/gc3ds
Parts list: https://docs.google.com/spreadsheets/d/1Zn6B7LS_Fjm-lIFg_gtSEX4vyMsDikPHXBdn-rX0lfU/edit#gid=0
3DS installation guide: https://docs.google.com/document/d/1vO6a_N2h2_0zf7rH5b2PBxTnxr9dSwcPrEzhVwxYAME/edit 
Sample modification gallery: http://imgur.com/a/YZrVK