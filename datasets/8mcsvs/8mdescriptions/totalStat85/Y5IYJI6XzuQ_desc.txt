Il Videoclip ufficiale del singolo dell'estate: "Il Pi Grande Spettacolo Dopo L'E3".

Una prestazione vocale in grado di competere con il gameplay di Lost Via Domus se non per la "firma" finale del Farenz, un contesto visivo inguardabile, reso speciale solo per pochi attimi dalla cara Alessia (che ringraziamo), ma si tratta comunque di un videoclip che vi segner tutti per la Vita, e che vorrete rivedere e riascoltare ad oltranza.

Vi ricordo che il singolo  scaricabile da iTunes: https://itunes.apple.com/it/podcast/the-gamefathers/id464104192?mt=2

Qui altrimenti potete ascoltare in streaming o scaricare la canzone: https://soundcloud.com/thegamefathers/il-piu-grande-spettacolo-dopo

I canali dei 4 Youtubers oggetto del video, che sicuramente conoscerete gi tutti, sono: 
Viperfritz: https://www.youtube.com/user/TheGameFathersITA (Questo!!)
Farenz: https://www.youtube.com/user/AngoloDiFarenz
Dr.Game: https://www.youtube.com/user/ThePhantomCastle
IlGatto: https://www.youtube.com/channel/UCNghxT0aJG9MAuZckBERT6A

A breve le Lyrics, ovvero il testo completo della canzone.
Per chi volesse Karaokare!