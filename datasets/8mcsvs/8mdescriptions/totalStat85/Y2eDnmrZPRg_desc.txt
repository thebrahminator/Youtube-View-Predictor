90 | Chuck Knows Church -- Ecumenical Sunday.  This day gives Christians an annual opportunity to continue their quest for the unity they already share in Christ. But, do you even know what it means?  Chuck will tell you on this his NINETIETH show!  

Chuck Knows Church is a WEEKLY online series.  Check back each Wednesday for the next episode!  Use each episode in worship, youth groups, fellowship groups, confirmation classes, worship teams, new member classes and late-night snack viewing!

Follow Chuck on his website:  www.ChuckKnowsChurch.com (You can also download Chuck shows here for free)

Facebook:  www.Facebook.com/ChuckKnowsChurch
Twitter:  @ChuckKnows

Chuck Knows Church is produced for all people by The General Board of Discipleship (GBOD) of The United Methodist Church.  

Please embed Chuck episodes on your website.  It's free, informative, and fun.