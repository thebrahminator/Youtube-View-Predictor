Dane makes his triumphant return to the featured stage with the introduction of the Icon Glove Set. Dane has made his mark on the gloving community -- gloving under several of well-known and established teams throughout his career. This iconic figure helped give professional insight on the glove set's design. The Icon glove set is both sleek and vibrant -- one of the most balanced glove sets for glovers of all skill levels.

This glove set is built with the following:
 
4 x MLtbLmg 3-Color Strobes
4 x MPuW 3-Color Strobes
 
Optional Thumbs:
 
2 x Turquoise Solid EVO-X
 
Upgrade all lights to EVO-X chips if you want more modes and to change the way the colors look.  Vivid Dane uses Blue MicroSkinz housings in the video.

Song: ARMNHMR ft. Jennifer Chung - A SIGHT SCENE (Original)
Song URL: http://www.youtube.com/watch?v=WxpZnrXvuws
ARMNHMR Facebook: https://www.facebook.com/armandhammermusic