import glob
import csv
import json


merged_lists = glob.glob('../datasets/8mcsvs/8mmerged/*.csv')
comment_per_subscribers = glob.glob('../datasets/8mcsvs/8mcommentpersubscriber/*.csv')
comment_per_views = glob.glob('../datasets/8mcsvs/8mcommentsbyview/*.csv')
dislikes_by_views = glob.glob('../datasets/8mcsvs/8mdislikesbyviews/*.csv')

dislikes_per_subscribers = glob.glob('../datasets/8mcsvs/8mdislikespersubscriber/*.csv')
elapsedtime_lists = glob.glob('../datasets/8mcsvs/8melapsedtime/*.csv')
likes_by_dislikes = glob.glob('../datasets/8mcsvs/8mlikesbydislikes/*.csv')
likes_by_subscribers = glob.glob('../datasets/8mcsvs/8mlikesbysubscriber/*.csv')

likes_by_views = glob.glob('../datasets/8mcsvs/8mlikesbyview/*.csv')
totalviews_by_totalsubscribers = glob.glob('../datasets/8mcsvs/8mtotalviewsbytotalsubscribers/*.csv')
totalviews_by_videocounts = glob.glob('../datasets/8mcsvs/8mtotalviewsbyvideocount/*.csv')
views_by_elapsedtimes = glob.glob('../datasets/8mcsvs/8mviewsbyelapsedtime/*.csv')

views_by_subscribers = glob.glob('../datasets/8mcsvs/8mviewsbysubscribers/*.csv')
channelelapsedtime_lists = glob.glob('../datasets/8mcsvs/8mchannelelapsed/*.csv')
totalviews_by_channelelpasedtimes = glob.glob('../datasets/8mcsvs/8mtotalviewsbychannelelapsed/*.csv')

allDataList = []
i = 0
for merged_list,comment_per_subscriber,comment_per_view,dislikes_by_view,dislikes_per_subscriber,elapsedtime_list, \
    likes_by_dislike, likes_by_subscriber, likes_by_view, totalviews_by_totalsubscriber, totalviews_by_videocount,\
        views_by_elapsedtime, views_by_subscriber, channelelapsedtime_list, totalviews_by_channelelpasedtime\
        in zip(merged_lists, comment_per_subscribers, comment_per_views,
               dislikes_by_views, dislikes_per_subscribers, elapsedtime_lists,
               likes_by_dislikes, likes_by_subscribers, likes_by_views,
               totalviews_by_totalsubscribers, totalviews_by_videocounts,
               views_by_elapsedtimes, views_by_subscribers,
               channelelapsedtime_lists, totalviews_by_channelelpasedtimes):

    merged_listFD = open(merged_list,'r')
    merged_listBrigde = csv.DictReader(merged_listFD)

    comment_per_subscriberFD = open(comment_per_subscriber,'r')
    comment_per_subscriberBridge = csv.DictReader(comment_per_subscriberFD)

    comment_per_viewFD = open(comment_per_view,'r')
    comment_per_viewBridge = csv.DictReader(comment_per_viewFD)

    dislikes_by_viewFD = open(dislikes_by_view,'r')
    dislikes_by_viewBridge = csv.DictReader(dislikes_by_viewFD)

    dislikes_per_subscriberFD = open(dislikes_per_subscriber,'r')
    dislikes_per_subscriberBridge = csv.DictReader(dislikes_per_subscriberFD)

    elapsedtime_listFD = open(elapsedtime_list,'r')
    elapsedtime_listBridge = csv.DictReader(elapsedtime_listFD)

    likes_by_dislikeFD = open(likes_by_dislike,'r')
    likes_by_dislikeBridge = csv.DictReader(likes_by_dislikeFD)

    likes_by_subscriberFD = open(likes_by_subscriber,'r')
    likes_by_subscriberBridge = csv.DictReader(likes_by_subscriberFD)

    likes_by_viewFD = open(likes_by_view,'r')
    likes_by_viewBridge = csv.DictReader(likes_by_viewFD)

    totalviews_by_totalsubscriberFD = open(totalviews_by_totalsubscriber, 'r')
    totalviews_by_totalsubscriberBridge = csv.DictReader(totalviews_by_totalsubscriberFD)

    totalviews_by_videocountFD = open(totalviews_by_videocount,'r')
    totalviews_by_videocountBridge = csv.DictReader(totalviews_by_videocountFD)

    views_by_elapsedtimeFD = open(views_by_elapsedtime,'r')
    views_by_elapsedtimeBridge = csv.DictReader(views_by_elapsedtimeFD)

    views_by_subscriberFD = open(views_by_subscriber,'r')
    views_by_subscriberBridge = csv.DictReader(views_by_subscriberFD)

    channelelapsedtime_listFD = open(channelelapsedtime_list, 'r')
    channelelapsedtime_listBridge = csv.DictReader(channelelapsedtime_listFD)

    totalviews_by_channelelpasedtimeFD = open(totalviews_by_channelelpasedtime,'r')
    totalviews_by_channelelpasedtimeBridge = csv.DictReader(totalviews_by_channelelpasedtimeFD)

    for merged_listD, comment_per_subscriberD, comment_per_viewD, dislikes_by_viewD, \
        dislikes_per_subscriberD, elapsedtime_listD, likes_by_dislikeD, likes_by_subscriberD,\
        likes_by_viewD, totalviews_by_totalsubscriberD, totalviews_by_videocountD, views_by_elapsedtimeD, \
            views_by_subscriberD, channelelapsedtime_listD, totalviews_by_channelelpasedtimeD \
            in zip(merged_listBrigde, comment_per_subscriberBridge, comment_per_viewBridge,
                                        dislikes_by_viewBridge, dislikes_per_subscriberBridge, elapsedtime_listBridge,
                                        likes_by_dislikeBridge, likes_by_subscriberBridge, likes_by_viewBridge,
                                        totalviews_by_totalsubscriberBridge, totalviews_by_videocountBridge,
                                        views_by_elapsedtimeBridge, views_by_subscriberBridge,
                   channelelapsedtime_listBridge, totalviews_by_channelelpasedtimeBridge):
        allDataDict = {}
        i = i+1
        print(i)
        allDataDict['videoId'] = merged_listD['videoId']
        allDataDict['channelId'] = merged_listD['channelId']
        allDataDict['videoPublished'] = merged_listD['videoPublished']
        allDataDict['channelViewCount'] = merged_listD['channelViewCount']
        allDataDict['channelCommentCount'] = merged_listD['channelCommentCount']
        allDataDict['subscriberCount'] = merged_listD['subscriberCount']
        allDataDict['videoCount'] = merged_listD['videoCount']
        allDataDict['videoCategoryId'] = merged_listD['videoCategoryId']
        allDataDict['videoViewCount'] = merged_listD['videoViewCount']
        allDataDict['videoLikeCount'] = merged_listD['videoLikeCount']
        allDataDict['videoDislikeCount'] = merged_listD['videoDislikeCount']
        allDataDict['VideoCommentCount'] = merged_listD['VideoCommentCount']


        allDataDict['comments/subscriber'] = comment_per_subscriberD['comments/subscriber']

        allDataDict['comments/views'] = comment_per_viewD['comments/views']

        allDataDict['dislikes/views'] = dislikes_by_viewD['dislikes/views']

        allDataDict['dislikes/subscriber'] = dislikes_per_subscriberD['dislikes/subscriber']

        allDataDict['elapsedtime'] = elapsedtime_listD['elapsedtime']

        allDataDict['likes/dislikes'] = likes_by_dislikeD['likes/dislikes']

        allDataDict['likes/subscriber'] = likes_by_subscriberD['likes/subscriber']

        allDataDict['likes/views'] = likes_by_viewD['likes/views']

        allDataDict['totviews/totsubs'] = totalviews_by_totalsubscriberD['totviews/totsubs']

        allDataDict['totvideos/videocount'] = totalviews_by_videocountD['totvideos/videocount']

        allDataDict['views/elapsedtime'] = views_by_elapsedtimeD['views/elapsedtime']

        allDataDict['views/subscribers'] = views_by_subscriberD['views/subscribers']

        allDataDict['channelelapsedtime'] = channelelapsedtime_listD['channelelapsedtime']

        allDataDict['totalviews/channelelapsedtime'] = \
            totalviews_by_channelelpasedtimeD['totalviews/channelelapsedtime']

        print(allDataDict)
        allDataList.append(allDataDict)

allDataJSON = json.dumps(allDataList)

with open('../datasets/YouTubeDataset_withChannelElapsed.json', 'w') as bridge:
    json.dump(allDataList,bridge)