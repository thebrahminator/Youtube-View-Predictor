<h1>Youtube Video Feature Predictor</h1>

<h2>Introduction</h2>
<p>In this project, I'm trying to build a standard system to predict the views
one would garner by putting their video on Youtube. The factors I'm taking into
consideration are the category ID, Likes, Dislikes, Views, Age of Channel etc. 
</p>

<h2>Server and Hosting Configuration</h2>
<p>
The site is hosted on and can be accessed by anyone.
Nginx has been used for the server, and Gunicorn is running inside a virtual environment</p>

