import pandas as pd
import numpy as np

channel_folder='./datasets/8mcsvs/8mchannels/'
video_folder='./datasets/8mcsvs/'
merged_folder='./datasets/8mcsvs/8mmerged/'


for file in range(77,87+1):
    print('channel ' + str(file))
    channels = pd.read_csv(channel_folder+'channelStats'+str(file)+'.csv')
    videos=pd.read_csv(video_folder+'videoStats'+str(file)+'.csv')

    merged=[]
    video_ids = videos['videoId'].tolist()

    videos=videos.values

    print('working...')

    c=0
    for i,channel in channels.iterrows():
        try:
            j=video_ids.index(channel['videoId'])
            video=videos[j]
            merged.append([channel['channelId'],channel['videoId'],channel['publishedAt'],video[1],channel['viewCount'],channel['commentCount'],channel['subscriberCount'],channel['videoCount'],video[2],video[3],video[4],video[5],video[7]])
            c+=1
        except ValueError:
            print('not there')

c=0
for i,channel in channels.iterrows():
    print(i)
    try:
        j=video_ids.index(channel['videoId'])
        video=videos[j]
        merged.append([channel['channelId'],channel['videoId'],
                       channel['publishedAt'],video[1],channel['viewCount'],
                       channel['commentCount'],channel['subscriberCount'],
                       channel['videoCount'],video[2],
                       video[3],video[4],video[5],video[7]])
        c+=1
    except ValueError:
        print('not there')

merged = pd.DataFrame(merged,columns = ['channelId','videoId','channelPublished',
                                        'videoPublished','channelViewCount','channelCommentCount',
                                        'subscriberCount','videoCount','videoCategoryId','videoViewCount',
                                        'videoLikeCount','videoDislikeCount','VideoCommentCount'])
merged.to_csv(merged_folder+'totalData1.csv')
print("completed")
merged = pd.DataFrame(merged,columns = ['channelId','videoId','channelPublished','videoPublished','channelViewCount','channelCommentCount','subscriberCount','videoCount','videoCategoryId','videoViewCount','videoLikeCount','videoDislikeCount','VideoCommentCount'])
merged.to_csv(merged_folder+'totalData'+str(file)+'.csv',index=False)
print("completed")
