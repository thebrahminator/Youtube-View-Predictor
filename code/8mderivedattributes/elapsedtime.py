import arrow
import glob
import csv

fileList = glob.glob('../datasets/8mcsvs/8mmerged/*.csv')

print(fileList)
for file in fileList:
    videoStatFD = open(file,'r')
    videoStatBridge = csv.DictReader(videoStatFD)

    fileName = file.split('/')
    impPart = fileName[4][-6:]
    if impPart[0] == 'a':
        impPart = impPart[-5:]
    print(impPart)
    current_time = arrow.now()
    writeFilePath = '../datasets/8mcsvs/8melapsedtime/elapsedtime' + impPart
    print("Starting " + writeFilePath)
    elapsedtimeFD = open(writeFilePath, 'w')
    headers = ("videoId", "channelId", "elapsedtime")
    elapsedtimeBridge = csv.DictWriter(elapsedtimeFD, headers)
    elapsedtimeBridge.writeheader()
    eltDict = {}
    for videoStat in videoStatBridge:
        eltDict["videoId"] = videoStat['videoId']
        eltDict["channelId"] = videoStat['channelId']
        publishedDate = arrow.get(videoStat['videoPublished'])
        elapsed_time = current_time - publishedDate
        hourselapsed,reminder = divmod(elapsed_time.days,3600)
        totalhours = elapsed_time.days*24 + hourselapsed
        eltDict["elapsedtime"] = totalhours
        elapsedtimeBridge.writerow(eltDict)

