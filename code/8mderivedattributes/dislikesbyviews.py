import glob
import csv

fileList = glob.glob('../datasets/8mcsvs/8mmerged/*.csv')
print(fileList)
for file in fileList:
    videoStatFD = open(file,'r')
    videoStatBridge = csv.DictReader(videoStatFD)

    fileName = file.split('/')
    impPart = fileName[4][-6:]
    if impPart[0] == 'a':
        impPart = impPart[-5:]
    print(impPart)

    writeFilePath = '../datasets/8mcsvs/8mdislikesbyviews/dislikesbyviews'+impPart
    print("Starting "+ writeFilePath)
    likesbyviewsFD = open(writeFilePath, 'w')
    headers = ("videoId", "dislikes/views")
    likesbyviewsBridge = csv.DictWriter(likesbyviewsFD, headers)
    likesbyviewsBridge.writeheader()
    lbvDict = {}
    for videoStat in videoStatBridge:
        lbvDict = {}
        lbvDict["videoId"] = videoStat['videoId']
        if int(videoStat['videoViewCount']) != 0 and int(videoStat['videoViewCount']) != -1 and \
                        int(videoStat['videoDislikeCount']) != -1:
            lbvDict["dislikes/views"] = float(int(videoStat['videoDislikeCount']) / int(videoStat['videoViewCount']))
        else:
            lbvDict["dislikes/views"] = -1

        likesbyviewsBridge.writerow(lbvDict)