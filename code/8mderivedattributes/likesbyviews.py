import glob
import csv

fileList = glob.glob('../datasets/8mcsvs/8mmerged/*.csv')
print(fileList)
for file in fileList:
    videoStatFD = open(file,'r')
    videoStatBridge = csv.DictReader(videoStatFD)

    fileName = file.split('/')
    impPart = fileName[4][-6:]
    if impPart[0] == 'a':
        impPart = impPart[-5:]
    print(impPart)

    writeFilePath = '../datasets/8mcsvs/8mlikesbyview/likesbyviews'+impPart
    print("Starting "+ writeFilePath)
    likesbyviewsFD = open(writeFilePath, 'w')
    headers = ("videoId", "likes/views")
    likesbyviewsBridge = csv.DictWriter(likesbyviewsFD, headers)
    likesbyviewsBridge.writeheader()
    lbvDict = {}
    for videoStat in videoStatBridge:
        lbvDict = {}
        lbvDict["videoId"] = videoStat['videoId']
        if int(videoStat['videoViewCount']) != 0 and int(videoStat['videoViewCount']) != -1 and \
                        int(videoStat['videoLikeCount']) != -1:
            lbvDict["likes/views"] = float(int(videoStat['videoLikeCount']) / int(videoStat['videoViewCount']))
        else:
            lbvDict["likes/views"] = -1

        likesbyviewsBridge.writerow(lbvDict)