import glob
import csv

fileList = glob.glob('../datasets/8mcsvs/8mmerged/*.csv')
print(fileList)
for file in fileList:
    videoStatFD = open(file,'r')
    videoStatBridge = csv.DictReader(videoStatFD)

    fileName = file.split('/')
    impPart = fileName[4][-6:]
    if impPart[0] == 'a':
        impPart = impPart[-5:]
    print(impPart)

    writeFilePath = '../datasets/8mcsvs/8mlikesbysubscriber/likespersubscriber'+impPart
    print("Starting "+ writeFilePath)
    commentsbyviewsFD = open(writeFilePath, 'w')
    headers = ("videoId","channelId", "likes/subscriber")
    commentsbyviewsBridge = csv.DictWriter(commentsbyviewsFD, headers)
    commentsbyviewsBridge.writeheader()
    cbvDict = {}
    for videoStat in videoStatBridge:
        cbvDict = {}
        cbvDict["videoId"] = videoStat['videoId']
        cbvDict["channelId"] = videoStat['channelId']
        if int(videoStat['subscriberCount']) != 0 and int(videoStat['subscriberCount']) != -1 and \
                        int(videoStat['videoLikeCount']) != -1:
            cbvDict["likes/subscriber"] = float(int(videoStat['videoLikeCount']) / int(videoStat['subscriberCount']))
        else:
            cbvDict["likes/subscriber"] = -1
        commentsbyviewsBridge.writerow(cbvDict)