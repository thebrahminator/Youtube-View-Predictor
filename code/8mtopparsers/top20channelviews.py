import json
import csv
from pprint import pprint


allStatsFD = open('../datasets/YouTubeDataset_withChannelElapsed.json', 'r')
allStatData = json.load(allStatsFD)

feature = "channelViewCount"
allVideoList = []

for data in allStatData:
    req_data = {}
    req_data['videoId'] = data['videoId']
    req_data['channelId'] = data['channelId']
    req_data[feature] = float(data[feature])
    allVideoList.append(req_data)

sortedAllVideoList = sorted(allVideoList, key=lambda k:k[feature], reverse=True)

top20DataPoints = []
finishedChannels = []
i = 0
for dataPoint in sortedAllVideoList:
    if dataPoint['channelId'] not in finishedChannels:
        top20DataPoints.append(dataPoint)
        finishedChannels.append(dataPoint['channelId'])
        i = i + 1
    else:
        continue
    if i == 20:
        break


pprint(top20DataPoints)
print(len(top20DataPoints))

requiredOutputFileFD = open('../datasets/8mcsvs/8mtops/top20channelview.csv', 'w')
headers = ('videoId', 'channelId', feature)
requiredOutputFileBride = csv.DictWriter(requiredOutputFileFD, headers)
requiredOutputFileBride.writeheader()


for component in top20DataPoints:
    requiredOutputFileBride.writerow(component)


print("Finished!!! :D ")